<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AdminSistem\DashboardController as DashboardAdminSistemController;
use App\Http\Controllers\AdminSistem\UserController;
use App\Http\Controllers\AdminInventori\KategoriController;
use App\Http\Controllers\AdminInventori\DashboardController as DashboardAdminInventoriController;
use App\Http\Controllers\AdminInventori\PenghapusanController as PenghapusanAdminInventoriController;
use App\Http\Controllers\AdminInventori\BarangController;
use App\Http\Controllers\AdminInventori\InventoriController;
use App\Http\Controllers\AdminInventori\RuanganController;
use App\Http\Controllers\AdminInventori\SupplierController;
use App\Http\Controllers\AdminInventori\PerbaikanController as PerbaikanAdminInventoriController;
use App\Http\Controllers\AdminInventori\PermintaanController as PerminaanAdminInventoriController;
use App\Http\Controllers\Karyawan\PengecekanController;
use App\Http\Controllers\Karyawan\DashboardController as DashboardKaryawanController;
use App\Http\Controllers\Karyawan\PerbaikanController;
use App\Http\Controllers\Karyawan\PermintaanController;
use App\Http\Controllers\Karyawan\PenghapusanController as PenghapusanKaryawanController;
use App\Http\Controllers\Manager\DashboardController as DashboardManagerController;
use App\Http\Controllers\Manager\PerbaikanController as PerbaikanManagerController;
use App\Http\Controllers\Manager\PermintaanController as PermintaanManagerController;
use App\Http\Controllers\Manager\PenghapusanController;
use App\Http\Controllers\Manager\Laporan\LaporanPermintaanController;
use App\Http\Controllers\Manager\Laporan\LaporanPengecekanController;
use App\Http\Controllers\Manager\Laporan\LaporanPerbaikanController;
use App\Http\Controllers\Manager\Laporan\LaporanPenghapusanController;
use App\Http\Controllers\Manager\Laporan\LaporanBarangController;
use App\Http\Controllers\Manager\Laporan\LaporanRuanganController;
use App\Http\Controllers\Manager\Laporan\LaporanInventoriController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return redirect('login');
});

Route::get('/clear',function(){
    \Artisan::call('config:cache');
    \Artisan::call('optimize');
    return 'success';
});

Route::group(['middleware' => ['guest-middleware']], function () {
    Route::get('login', [LoginController::class, 'index']);
    Route::post('login', [LoginController::class, 'login']);

});

Route::group(['middleware' => ['auth-middleware']], function () {
    Route::get('logout', [LoginController::class, 'logout']);

    // routing hak akses admin-sistem
    Route::group(['middleware' => ['admin-sistem']], function () {
        Route::group(['prefix' => 'admin-sistem'], function () {
            Route::get('dashboard', [DashboardAdminSistemController::class, 'index']);

            Route::get('/user', [UserController::class, 'index']);
            Route::post('/create-user', [UserController::class, 'store']);
            Route::patch('/edit-user/{user}', [UserController::class, 'update']);
            Route::delete('/delete-user/{user}', [UserController::class, 'destroy']);
        });
    });

    // routing hak akses admin-inventori
    Route::group(['middleware' => ['admin-inventori']], function () {
        Route::group(['prefix' => 'admin-inventori'], function () {
            Route::get('dashboard', [DashboardAdminInventoriController::class, 'index']);

            // route perbaikan
            Route::get('/perbaikan', [PerbaikanAdminInventoriController::class, 'index']);
            Route::patch('/edit-perbaikan/{perbaikan}', [PerbaikanAdminInventoriController::class, 'update']);
            Route::delete('/delete-perbaikan/{perbaikan}', [PerbaikanAdminInventoriController::class, 'destroy']);

            Route::get('detail/perbaikan/{id}', [PerbaikanAdminInventoriController::class, 'detail']);
            Route::patch('detail/edit-perbaikan/{perbaikan}', [PerbaikanAdminInventoriController::class, 'detail_update']);
            Route::delete('detail/delete-perbaikan/{perbaikan}', [PerbaikanAdminInventoriController::class, 'detail_destroy']);

            // route permintaan
            Route::get('permintaan', [PerminaanAdminInventoriController::class, 'permintaan']);
            Route::patch('permintaan-edit/{permintaan}', [PerminaanAdminInventoriController::class, 'update']);
            Route::delete('permintaan-delete/{permintaan}', [PerminaanAdminInventoriController::class, 'destroy']);

            Route::get('detail/permintaan/{id}', [PerminaanAdminInventoriController::class, 'index']);
            Route::patch('detail/edit-permintaan/{permintaan}', [PerminaanAdminInventoriController::class, 'update_detail']);
            Route::delete('detail/delete-permintaan/{permintaan}', [PerminaanAdminInventoriController::class, 'destroy_detail']);

            // route Kategori
            Route::get('/kategori', [KategoriController::class, 'index']);
            Route::post('/create-kategori', [KategoriController::class, 'store']);
            Route::patch('/edit-kategori/{kategori}', [KategoriController::class, 'update']);
            Route::delete('/delete-kategori/{kategori}', [KategoriController::class, 'destroy']);

            Route::get('/barang', [BarangController::class, 'index']);
            Route::post('/create-barang', [BarangController::class, 'store']);
            Route::patch('/edit-barang/{barang}', [BarangController::class, 'update']);
            Route::delete('/delete-barang/{barang}', [BarangController::class, 'destroy']);

            Route::get('/supplier', [SupplierController::class, 'index']);
            Route::post('/create-supplier', [SupplierController::class, 'store']);
            Route::patch('/edit-supplier/{supplier}', [SupplierController::class, 'update']);
            Route::delete('/delete-supplier/{supplier}', [SupplierController::class, 'destroy']);

            Route::get('/ruangan', [RuanganController::class, 'index']);
            Route::post('/create-ruangan', [RuanganController::class, 'store']);
            Route::patch('/edit-ruangan/{ruangan}', [RuanganController::class, 'update']);
            Route::delete('/delete-ruangan/{ruangan}', [RuanganController::class, 'destroy']);

            Route::get('/inventori', [InventoriController::class, 'index']);
            Route::post('/create-inventori', [InventoriController::class, 'store']);
            Route::patch('/edit-inventori/{inventori}', [InventoriController::class, 'update']);
            Route::delete('/delete-inventori/{inventori}', [InventoriController::class, 'destroy']);

            // route penghapusan
            Route::get('/transaksi-penghapusan', [PenghapusanAdminInventoriController::class, 'index']);
            Route::post('/create-detail-penghapusan', [PenghapusanAdminInventoriController::class, 'store']);
            Route::patch('/edit-penghapusan/{penghapusan}', [PenghapusanAdminInventoriController::class, 'update']);
            Route::delete('/delete-penghapusan/{penghapusan}', [PenghapusanAdminInventoriController::class, 'destroy']);
        });
    });

    // routing hak akses manager
    Route::group(['middleware' => ['manager']], function () {
        Route::group(['prefix' => 'manager'], function () {
            Route::get('dashboard', [DashboardManagerController::class, 'index']);

            Route::group(['prefix' => 'transaksi'], function () {
                Route::get('/transaksi-permintaan', [PermintaanManagerController::class, 'index']);
                Route::patch('/transaksi-edit-permintaan/{permintaan}', [PermintaanManagerController::class, 'update']);
                Route::delete('/transaksi-delete-permintaan/{permintaan}', [PermintaanManagerController::class, 'destroy']);
                Route::get('/transaksi-permintaan/detail/{id}', [PermintaanManagerController::class, 'detail']);
                Route::patch('/transaksi-edit-permintaan/detail/{id}', [PermintaanManagerController::class, 'update_detail']);
                Route::delete('/transaksi-delete-permintaan/detail/{id}', [PermintaanManagerController::class, 'destroy_detail']);

                Route::get('/transaksi-perbaikan', [PerbaikanManagerController::class, 'index']);
                Route::patch('/transaksi-edit-perbaikan/{perbaikan}', [PerbaikanManagerController::class, 'update']);
                Route::delete('/transaksi-delete-perbaikan/{perbaikan}', [PerbaikanManagerController::class, 'destroy']);

                // Route::get('/transaksi-penghapusan', [PenghapusanManagerController::class, 'index']);
                // Route::patch('/transaksi-edit-penghapusan/{penghapusan}', [PenghapusanManagerController::class, 'update']);
                // Route::delete('/transaksi-delete-penghapusan/{penghapusan}', [PenghapusanManagerController::class, 'destroy']);

                Route::get('/transaksi-penghapusan', [PenghapusanController::class, 'index']);
                Route::post('/create-detail-penghapusan', [PenghapusanController::class, 'store']);
                Route::patch('/edit-penghapusan/{penghapusan}', [PenghapusanController::class, 'update']);
                Route::delete('/delete-penghapusan/{penghapusan}', [PenghapusanController::class, 'destroy']);
            });

            Route::group(['prefix' => 'laporan'], function () {
                Route::get('/permintaan', [LaporanPermintaanController::class, 'index']);
                Route::post('/cari-permintaan', [LaporanPermintaanController::class, 'cari']);
                Route::get('/cetak-permintaan/{tanggal_awal}/{tanggal_akhir}', [LaporanPermintaanController::class, 'cetak']);

                Route::get('/pengecekan', [LaporanPengecekanController::class, 'index']);
                Route::post('/cari-pengecekan', [LaporanPengecekanController::class, 'cari']);
                Route::get('/cetak-pengecekan/{tanggal_awal}/{tanggal_akhir}', [LaporanPengecekanController::class, 'cetak']);

                Route::get('/perbaikan', [LaporanPerbaikanController::class, 'index']);
                Route::post('/cari-perbaikan', [LaporanPerbaikanController::class, 'cari']);
                Route::get('/cetak-perbaikan/{tanggal_awal}/{tanggal_akhir}', [LaporanPerbaikanController::class, 'cetak']);

                Route::get('/penghapusan', [LaporanPenghapusanController::class, 'index']);
                Route::post('/cari-penghapusan', [LaporanPenghapusanController::class, 'cari']);
                Route::get('/cetak-penghapusan/{tanggal_awal}/{tanggal_akhir}', [LaporanPenghapusanController::class, 'cetak']);

                Route::get('/barang', [LaporanBarangController::class, 'index']);
                Route::get('/cetak-barang', [LaporanBarangController::class, 'cetak']);

                Route::get('/ruangan', [LaporanRuanganController::class, 'index']);
                Route::get('/cetak-ruangan', [LaporanRuanganController::class, 'cetak']);

                Route::get('/inventori', [LaporanInventoriController::class, 'index']);
                Route::get('/cetak-inventori', [LaporanInventoriController::class, 'cetak']);
            });
        });
    });

    // routing hak akses karyawan
    Route::group(['middleware' => ['karyawan']], function () {
        Route::group(['prefix' => 'karyawan'], function () {
            Route::get('dashboard', [DashboardKaryawanController::class, 'index']);

            Route::group(['prefix' => 'transaksi'], function () {
                Route::get('/permintaan', [PermintaanController::class, 'index']);
                Route::post('/create-detail-permintaan', [PermintaanController::class, 'store']);
                Route::patch('/edit-permintaan/{permintaan}', [PermintaanController::class, 'update']);
                Route::delete('/delete-permintaan/{permintaan}', [PermintaanController::class, 'destroy']);

                Route::get('/perbaikan', [PerbaikanController::class, 'index']);
                Route::post('/create-detail-perbaikan', [PerbaikanController::class, 'store']);
                Route::patch('/edit-perbaikan/{perbaikan}', [PerbaikanController::class, 'update']);
                Route::delete('/delete-perbaikan/{perbaikan}', [PerbaikanController::class, 'destroy']);

                Route::get('/pengecekan', [PengecekanController::class, 'index']);
                Route::post('/create-detail-pengecekan', [PengecekanController::class, 'store']);
                Route::patch('/edit-pengecekan/{pengecekan}', [PengecekanController::class, 'update']);
                Route::delete('/delete-pengecekan/{pengecekan}', [PengecekanController::class, 'destroy']);

                Route::get('/penghapusan', [PenghapusanKaryawanController::class, 'index']);
                Route::post('/create-detail-penghapusan', [PenghapusanKaryawanController::class, 'store']);
                Route::patch('/edit-penghapusan/{id}', [PenghapusanKaryawanController::class, 'update']);
                Route::delete('/delete-penghapusan/{id}', [PenghapusanKaryawanController::class, 'destroy']);
            });
        });
    });
});
