<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class GuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::guard('admin-sistem')->check()){
            return redirect('admin-sistem/dashboard');
        }
        elseif(Auth::guard('admin-inventori')->check()){
            return redirect('admin-inventori/dashboard');
        }
        elseif(Auth::guard('manager')->check()){
            return redirect('manager/dashboard');
        }
        elseif(Auth::guard('karyawan')->check()){
            return redirect('karyawan/dashboard');
        }
        else{
            return $next($request);
        }
    }
}
