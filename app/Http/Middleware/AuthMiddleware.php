<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(\Auth::guard('admin-sistem')->check()){
            return $next($request);
        }
        elseif(\Auth::guard('admin-inventori')->check()){
            return $next($request);
        }
        elseif(\Auth::guard('manager')->check()){
            return $next($request);
        }
        elseif(\Auth::guard('karyawan')->check()){
            return $next($request);
        }
        else{
            return redirect('/login');
        }
    }
}
