<?php

namespace App\Http\Controllers\AdminSistem;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $user = User::all();
        return view('admin-sistem.user.index', compact('user'));
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->nama_lengkap = $request->nama_lengkap;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->level = $request->level;
        $user->no_hp = $request->no_telp;

        $user->save();
        return redirect('/admin-sistem/user')->with('status', 'Data Admin Berhasil Ditambahkan !');
    }

    public function update(Request $request, User $user)
    {
        User::where('id', $user->id)
            ->update([
                'nama_lengkap' => $request->nama_lengkap,
                'username' => $request->username,
                'level' => $request->level,
                'no_hp' => $request->no_telp,
            ]);
        return redirect('/admin-sistem/user')->with('status', 'Data User Berhasil Diedit!');
    }

    public function destroy(User $user)
    {
        $delete_user = $user->id;
        User::destroy($delete_user);
        return redirect('/admin-sistem/user')->with('statusdel', 'Data User Berhasil Dihapus!');
    }
}
