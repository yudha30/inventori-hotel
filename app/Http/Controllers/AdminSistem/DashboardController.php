<?php

namespace App\Http\Controllers\AdminSistem;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        $admin_sistem = User::where('level',1)->count();
        $admin_inventori = User::where('level',2)->count();
        $manager = User::where('level',3)->count();
        $karyawan = User::where('level',4)->count();

        return view('admin-sistem.dashboard.index',compact('admin_sistem','admin_inventori','manager','karyawan'));
    }
}
