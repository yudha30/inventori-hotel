<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

use Redirect;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.index');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $login = [
            'username' => $request->username,
            'password' => $request->password
        ];

        $check = DB::table('tb_user')
            ->where('username', $login['username'])
            ->whereNull('deleted_at')
            ->first();

        if(!$check){
            return Redirect::back()->with('error', 'Login gagal!, periksa username dan password anda.');
        }

        if(Hash::check($login['password'], $check->password)){
            if ($check->level == 1) {
                Auth::guard('admin-sistem')->attempt($login);
                return redirect('admin-sistem/dashboard');
            }
            elseif($check->level == 2){
                Auth::guard('admin-inventori')->attempt($login);
                return redirect('admin-inventori/dashboard');
            }
            elseif($check->level == 3){
                Auth::guard('manager')->attempt($login);
                return redirect('manager/dashboard');
            }
            else{
                Auth::guard('karyawan')->attempt($login);
                return redirect('karyawan/dashboard');
            }
        }else{
            return Redirect::back()->with('error', 'Login gagal!, periksa username dan password anda.');
        }
    }

    public function logout()
    {
        if(Auth::guard('admin-sistem')->check()) {
            Auth::guard('admin-sistem')->logout();
        }
        if(Auth::guard('admin-inventori')->check()){
            Auth::guard('admin-inventori')->logout();
        }
        if(Auth::guard('manager')->check()){
            Auth::guard('manager')->logout();
        }
        if(Auth::guard('karyawan')->check()){
            Auth::guard('karyawan')->logout();
        }

        return redirect('login');
    }
}
