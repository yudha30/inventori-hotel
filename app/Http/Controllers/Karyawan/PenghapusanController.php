<?php

namespace App\Http\Controllers\Karyawan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Penghapusan;
use App\Models\DetailPenghapusan;
use App\Models\Inventori;
use App\Models\Barang;
use App\Models\Ruangan;
use Auth;

class PenghapusanController extends Controller
{
    public function index()
    {
        $tb_inventori = Inventori::where('tersedia',1)
            ->where('keadaan_barang','Rusak')->get();

        $penghapusan = DB::table('tb_penghapusan')
            ->select([
                'tb_penghapusan.id',
                'tb_penghapusan.user_id',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_penghapusan.user_id','=','tb_user.id');

        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $detail_penghapusan = DB::table('tb_detail_penghapusan')
            ->select([
                'tb_detail_penghapusan.*',
                'tb_penghapusan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_penghapusan.user_id',

            ])
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_penghapusan.inventori_id', '=', 'tb_inventori.id');
            })
            ->joinSub($penghapusan, 'tb_penghapusan', function ($join) {
                $join->on('tb_detail_penghapusan.penghapusan_id', '=', 'tb_penghapusan.id');
            })
            ->where('tb_penghapusan.user_id',Auth::guard('karyawan')->user()->id)
            ->whereNull('tb_detail_penghapusan.deleted_at')
            ->paginate(15);

        return view('karyawan.penghapusan', compact('tb_inventori','detail_penghapusan'));
    }

    public function store(Request $request)
    {
        $penghapusan = new Penghapusan;
        $penghapusan->user_id = $request->id_user;
        $penghapusan->tgl_penghapusan = now();
        $penghapusan->save();

        foreach ($request->id_inventori as $key => $data){
            $detail_penghapusan = new DetailPenghapusan;
            $detail_penghapusan->penghapusan_id = $penghapusan->id;
            $detail_penghapusan->inventori_id = $data;
            $detail_penghapusan->keterangan = "Pengajuan Penghapusan";
            $detail_penghapusan->save();
        };

        return redirect('/karyawan/transaksi/penghapusan')->with('status', 'Data Penghapusan Berhasil Ditambahkan !');
    }

    public function update(Request $request, $id)
    {
        DetailPenghapusan::where('id', $id)
        ->update([
            'inventori_id' => $request->id_inventori,
        ]);
        return redirect('/karyawan/transaksi/penghapusan')->with('status', 'Data Penghapusan Berhasil Diubah !');
    }

    public function destroy($id)
    {
        $detail_penghapusan = DetailPenghapusan::find($id);

        $jumlah_detail_penghapusan = DetailPenghapusan::where('penghapusan_id', $detail_penghapusan->penghapusan_id)->get();
        $total_detail_penghapusan = count($jumlah_detail_penghapusan);

        if ($total_detail_penghapusan > 1) {
            DetailPenghapusan::destroy($id);
            return redirect('/karyawan/transaksi/penghapusan')->with('statusdel', 'Data Penghapusan Berhasil Dihapus!');
        }else {
            Detailpenghapusan::destroy($id);
            penghapusan::destroy($detail_penghapusan->penghapusan_id);
            return redirect('/karyawan/transaksi/penghapusan')->with('statusdel', 'Data Penghapusan Berhasil Dihapus!');
        }
    }
}
