<?php

namespace App\Http\Controllers\Karyawan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Permintaan;
use App\Models\Pengecekan;
use App\Models\Perbaikan;

use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $perbaikan = Perbaikan::where('user_id',Auth::guard('karyawan')->user()->id)->count();
        $permintaan = Permintaan::where('user_id',Auth::guard('karyawan')->user()->id)->count();
        $pengecekan = Pengecekan::where('user_id',Auth::guard('karyawan')->user()->id)->count();

        return view('karyawan.dashboard',compact('pengecekan','perbaikan','permintaan'));
    }
}
