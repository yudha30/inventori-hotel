<?php

namespace App\Http\Controllers\Karyawan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Pengecekan;
use App\Models\DetailPengecekan;
use App\Models\Inventori;

use Auth;

class PengecekanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tb_inventori = Inventori::all();
        // foreach ($inventori as $key => $value) {
        //     dd($value->id);
        // }
        // $pengecekan = Pengecekan::all();
        // $detail_pengecekan = DetailPengecekan::all();

        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $pengecekan = DB::table('tb_pengecekan')
            ->select([
                'tb_pengecekan.id',
                'tb_pengecekan.user_id',
                'tb_pengecekan.tgl_pengecekan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_pengecekan.user_id','=','tb_user.id');

        $detail_pengecekan = DB::table('tb_detail_pengecekan')
            ->select([
                'tb_detail_pengecekan.*',
                'tb_pengecekan.tgl_pengecekan',
                'tb_pengecekan.nama_lengkap',
                'tb_pengecekan.user_id',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($pengecekan, 'tb_pengecekan', function ($join) {
                $join->on('tb_detail_pengecekan.pengecekan_id', '=', 'tb_pengecekan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_pengecekan.inventori_id', '=', 'tb_inventori.id');
            })
            ->where('tb_pengecekan.user_id',Auth::guard('karyawan')->user()->id)
            ->whereNull('tb_detail_pengecekan.deleted_at')
            ->orderByDesc('tb_detail_pengecekan.updated_at')
            ->get();
        return view('karyawan.pengecekan', compact('tb_inventori','detail_pengecekan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $pengecekan = new Pengecekan;
        $pengecekan->user_id = $request->id_user;
        $pengecekan->tgl_pengecekan = now();

        $pengecekan->save();

        $last_id_pengecekan = Pengecekan::all()->last();

        foreach($request->id_inventori as $key => $data)
        {

            $inventori = Inventori::find($data)
            ->update([
                'keadaan_barang' => $request->keadaan_barang[$key],
            ]);

            $detail_pengecekan = new DetailPengecekan;
            $detail_pengecekan->pengecekan_id = $last_id_pengecekan->id;
            $detail_pengecekan->inventori_id = $data;
            $detail_pengecekan->keterangan = $request->keterangan[$key];

            $detail_pengecekan->save();
        };

        return redirect('/karyawan/transaksi/pengecekan')->with('status', 'Data Pengecekan Berhasil Ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DetailPengecekan::where('id', $id)
        ->update([
            'inventori_id' => $request->id_inventori,
            'keterangan' => $request->keterangan,
        ]);
        return redirect('/karyawan/transaksi/pengecekan')->with('status', 'Data Pengecekan Berhasil Diubah !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail_pengecekan = DetailPengecekan::where('id', $id)->get();
        $jumlah_detail_pengecekan = DetailPengecekan::where('pengecekan_id', $detail_pengecekan[0]->pengecekan_id)->get();
        $total_detail_pengecekan = count($jumlah_detail_pengecekan);

        if ($total_detail_pengecekan != 1) {
            DetailPengecekan::destroy($id);
            return redirect('/karyawan/transaksi/pengecekan')->with('statusdel', 'Data Pengecekan Berhasil Dihapus!');
        }else {
            DetailPengecekan::destroy($id);
            Pengecekan::destroy($detail_pengecekan[0]->pengecekan_id);
            return redirect('/karyawan/transaksi/pengecekan')->with('statusdel', 'Data Pengecekan Berhasil Dihapus!');
        }
    }
}
