<?php

namespace App\Http\Controllers\Karyawan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Perbaikan;
use App\Models\DetailPerbaikan;
use App\Models\Inventori;
use Illuminate\Support\Facades\DB;

use Auth;

class PerbaikanController extends Controller
{
    public function index()
    {
        $tb_inventori = Inventori::where('tersedia',true)
            ->where('keadaan_barang','Rusak')
            ->get();

        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $perbaikan = DB::table('tb_perbaikan')
            ->select([
                'tb_perbaikan.id',
                'tb_perbaikan.status as status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.user_id',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_perbaikan.user_id','=','tb_user.id');

        $detail_perbaikan = DB::table('tb_detail_perbaikan')
            ->select([
                'tb_detail_perbaikan.*',
                'tb_perbaikan.status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_perbaikan.user_id',
            ])
            ->leftJoinSub($perbaikan, 'tb_perbaikan', function ($join) {
                $join->on('tb_detail_perbaikan.perbaikan_id', '=', 'tb_perbaikan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_perbaikan.inventori_id', '=', 'tb_inventori.id');
            })
            ->where('tb_perbaikan.user_id',Auth::guard('karyawan')->user()->id)
            ->whereNull('tb_detail_perbaikan.deleted_at')
            ->orderByDesc('tb_detail_perbaikan.updated_at')
            ->get();
        return view('karyawan.perbaikan.perbaikan', compact('tb_inventori','detail_perbaikan'));
    }

    public function store(Request $request)
    {
        $perbaikan = new Perbaikan;
        $perbaikan->user_id = $request->id_user;
        $perbaikan->status = "Perbaikan";
        $perbaikan->tgl_perbaikan = now();

        $perbaikan->save();

        $last_id_perbaikan = Perbaikan::all()->last();

        for ($i=0; $i < count($request->id_inventori) ; $i++) {
            $detail_perbaikan = new DetailPerbaikan;
            $detail_perbaikan->perbaikan_id = $last_id_perbaikan->id;
            $detail_perbaikan->inventori_id = $request->id_inventori[$i];
            $detail_perbaikan->keterangan = $request->keterangan[$i];
            $detail_perbaikan->status = "-";
            $detail_perbaikan->save();

            $inventori = Inventori::find($request->id_inventori[$i])->update([
                'status' => 'Rusak'
            ]);
        };

        return redirect('/karyawan/transaksi/perbaikan')->with('status', 'Data Perbaikan Berhasil Ditambahkan !');
    }

    public function update(Request $request, $id)
    {
        DetailPerbaikan::find($id)
        ->update([
            'keterangan' => $request->keterangan,
        ]);
        return redirect('/karyawan/transaksi/perbaikan')->with('status', 'Data Perbaikan Berhasil Diubah !');
    }

    public function destroy($id)
    {
        $detail_perbaikan = DetailPerbaikan::where('id', $id)->get();
        $jumlah_detail_perbaikan = DetailPerbaikan::where('perbaikan_id', $detail_perbaikan[0]->perbaikan_id)->get();
        $total_detail_perbaikan = count($jumlah_detail_perbaikan);

        if ($total_detail_perbaikan != 1) {
            DetailPerbaikan::destroy($id);
            return redirect('/karyawan/transaksi/perbaikan')->with('statusdel', 'Data Perbaikan Berhasil Dihapus!');
        }else {
            DetailPerbaikan::destroy($id);
            Perbaikan::destroy($detail_perbaikan[0]->perbaikan_id);
            return redirect('/karyawan/transaksi/perbaikan')->with('statusdel', 'Data Perbaikan Berhasil Dihapus!');
        }
    }
}
