<?php

namespace App\Http\Controllers\Karyawan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\Permintaan;
use App\Models\DetailPermintaan;
use Illuminate\Support\Facades\DB;

use Auth;

class PermintaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $detail = DB::table('tb_detail_permintaan')
            ->select([
                'tb_detail_permintaan.id',
                'tb_detail_permintaan.permintaan_id',
                'tb_detail_permintaan.status_perubahan_permintaan',
                'tb_barang.nama_barang',
            ])
            ->leftJoin('tb_barang','tb_detail_permintaan.barang_id','=','tb_barang.id');

        $status = DB::table('tb_permintaan')
            ->select([
                'tb_detail_permintaan.nama_barang'
            ])
            ->leftJoinSub($detail, 'tb_detail_permintaan', function ($join) {
                $join->on('tb_permintaan.id', '=', 'tb_detail_permintaan.permintaan_id');
            })
            ->where([
                'tb_detail_permintaan.status_perubahan_permintaan' => 1,
                'tb_permintaan.status' => 'Acc Manager'
            ])
            ->whereNull('tb_permintaan.deleted_at')
            ->get();

        $barang = Barang::all();
        // $permintaan = Permintaan::all();
        // $detail_permintaan = DetailPermintaan::all()->sortByDesc('created_at');

        $permintaan = DB::table('tb_permintaan')
            ->select([
                'tb_permintaan.id',
                'tb_permintaan.status as status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.user_id',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_permintaan.user_id','=','tb_user.id');

        $detail_permintaan = DB::table('tb_detail_permintaan')
            ->select([
                'tb_detail_permintaan.*',
                'tb_barang.nama_barang',
                'tb_supplier.nama_supplier',
                'tb_permintaan.status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.nama_lengkap',
                'tb_permintaan.user_id',
            ])
            ->leftJoin('tb_barang','tb_detail_permintaan.barang_id','=','tb_barang.id')
            ->leftJoin('tb_supplier','tb_detail_permintaan.supplier_id','=','tb_supplier.id')
            ->leftJoinSub($permintaan, 'tb_permintaan', function ($join) {
                $join->on('tb_detail_permintaan.permintaan_id', '=', 'tb_permintaan.id');
            })
            ->where('tb_permintaan.user_id',Auth::guard('karyawan')->user()->id)
            ->whereNull('tb_detail_permintaan.deleted_at')
            ->orderByDesc('tb_detail_permintaan.updated_at')
            ->get();
        return view('karyawan.permintaan', compact('barang','detail_permintaan','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permintaan = new Permintaan;
        $permintaan->user_id = $request->id_user;
        $permintaan->tgl_permintaan = now();
        $permintaan->status = "Permintaan";

        $permintaan->save();

        $last_id_permintaan = Permintaan::all()->last();

        for ($i=0; $i < count($request->jumlah) ; $i++) {
            $detail_permintaan = new DetailPermintaan;
            $detail_permintaan->permintaan_id = $last_id_permintaan->id;
            $detail_permintaan->supplier_id = null;
            $detail_permintaan->barang_id = $request->id_barang[$i];
            $detail_permintaan->jumlah_permintaan = $request->jumlah[$i];
            $detail_permintaan->status = "-";

            $detail_permintaan->save();
        };

        return redirect('/karyawan/transaksi/permintaan')->with('status', 'Data Permintaan Berhasil Ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DetailPermintaan::where('id', $id)
        ->update([
            'jumlah_permintaan' => $request->jumlah,
        ]);
        return redirect('/karyawan/transaksi/permintaan')->with('status', 'Data Permintaan Berhasil Diubah !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail_permintaan = DetailPermintaan::where('id', $id)->get();
        $jumlah_detail_permintaan = DetailPermintaan::where('permintaan_id', $detail_permintaan[0]->permintaan_id)->get();
        $total_detail_permintaan = count($jumlah_detail_permintaan);

        if ($total_detail_permintaan != 1) {
            DetailPermintaan::destroy($id);
            return redirect('/karyawan/transaksi/permintaan')->with('statusdel', 'Data Permintaan Berhasil Dihapus!');
        }else {
            DetailPermintaan::destroy($id);
            Permintaan::destroy($detail_permintaan[0]->permintaan_id);
            return redirect('/karyawan/transaksi/permintaan')->with('statusdel', 'Data Permintaan Berhasil Dihapus!');
        }
    }
}
