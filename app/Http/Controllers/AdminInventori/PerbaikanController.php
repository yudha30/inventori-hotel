<?php

namespace App\Http\Controllers\AdminInventori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Perbaikan;
use App\Models\DetailPerbaikan;
use App\Models\Inventori;

class PerbaikanController extends Controller
{
    public function index()
    {
        $inventori = Inventori::all();
        $perbaikan = Perbaikan::all();
        return view('admin-inventori.perbaikan.perbaikan', compact('inventori','perbaikan'));
    }

    public function update(Request $request, $id)
    {
        $update = Perbaikan::find($id);
        $update->update([
            'status'=> $request->status
        ]);

        return redirect()->back()->with('status', 'Data Perbaikan Berhasil Diubah !');
    }

    // detail
    public function detail($id)
    {
        $inventori = Inventori::all();
        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $perbaikan = DB::table('tb_perbaikan')
            ->select([
                'tb_perbaikan.id',
                'tb_perbaikan.status as status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_perbaikan.user_id','=','tb_user.id');

        $detail_perbaikan = DB::table('tb_detail_perbaikan')
            ->select([
                'tb_detail_perbaikan.*',
                'tb_perbaikan.status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($perbaikan, 'tb_perbaikan', function ($join) {
                $join->on('tb_detail_perbaikan.perbaikan_id', '=', 'tb_perbaikan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_perbaikan.inventori_id', '=', 'tb_inventori.id');
            })
            ->where('tb_detail_perbaikan.perbaikan_id',$id)
            ->whereNull('tb_detail_perbaikan.deleted_at')
            ->orderByDesc('tb_detail_perbaikan.updated_at')
            ->get();
        return view('admin-inventori.perbaikan.detail_perbaikan', compact('inventori','detail_perbaikan'));
    }

    public function detail_update(Request $request, $id)
    {
        $detail_perbaikan = DetailPerbaikan::find($id);
        $detail_perbaikan->update([
            'status' => $request->status,
        ]);

        if($request->status=='Selesai Diperbaiki'){
            Inventori::find($detail_perbaikan->inventori_id)
                ->update([
                    'keadaan_barang' => 'Baik'
                ]);
        }

        return redirect()->back()->with('status', 'Data Perbaikan Berhasil Diubah !');
    }

    public function detail_destroy($id)
    {
        $detail_perbaikan = DetailPerbaikan::where('id', $id)->get();
        $jumlah_detail_perbaikan = DetailPerbaikan::where('perbaikan_id', $detail_perbaikan[0]->perbaikan_id)->get();
        $total_detail_perbaikan = count($jumlah_detail_perbaikan);

        if ($total_detail_perbaikan != 1) {
            DetailPerbaikan::destroy($id);
            return redirect()->back()->with('statusdel', 'Data Perbaikan Berhasil Dihapus!');
        }else {
            DetailPerbaikan::destroy($id);
            Perbaikan::destroy($detail_perbaikan[0]->perbaikan_id);
            return redirect()->back()->with('statusdel', 'Data Perbaikan Berhasil Dihapus!');
        }
    }
}
