<?php

namespace App\Http\Controllers\AdminInventori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Kategori;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori = Kategori::paginate(15);
        return view('admin-inventori.kategori', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategori = DB::table('tb_kategori')
            ->where('nama_kategori',$request->nama_kategori)
            ->whereNull('deleted_at')
            ->first();
        if($kategori){
            return redirect()->back()->with('statusdel', 'Nama kategori sudah ada sebelumnya!');
        }else{
            $kategori = new Kategori;
            $kategori->nama_kategori = $request->nama_kategori;

            $kategori->save();
            return redirect('/admin-inventori/kategori')->with('status', 'Data Kategori Berhasil Ditambahkan !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Kategori::where('id', $id)
            ->update([
                'nama_kategori' => $request->nama_kategori,
            ]);
            return redirect('/admin-inventori/kategori')->with('status', 'Data Kategori Berhasil Diubah !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kategori::destroy($id);
        return redirect('/admin-inventori/kategori')->with('statusdel', 'Data Kategori Berhasil Dihapus!');
    }
}
