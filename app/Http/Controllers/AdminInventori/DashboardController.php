<?php

namespace App\Http\Controllers\AdminInventori;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Perbaikan;
use App\Models\Permintaan;

class DashboardController extends Controller
{
    public function index()
    {
        $permintaan = Permintaan::all()->count();
        $perbaikan = Perbaikan::all()->count();
        
        return view('admin-inventori.dashboard', compact('perbaikan','permintaan'));
    }
}
