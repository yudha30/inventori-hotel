<?php

namespace App\Http\Controllers\AdminInventori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Inventori;
use App\Models\Barang;
use App\Models\Ruangan;

class InventoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::all();
        // $inventori = Inventori::where('tersedia',true)->paginate(15)

        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.*',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id')
            ->where('tersedia',true)
            ->paginate(15);
        $ruangan = Ruangan::all();
        return view('admin-inventori.inventori', compact('barang','inventori','ruangan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_inventori' => 'required|unique:tb_inventori',
        ]);

        if(!empty($request->id_barang)){

            $barang = Barang::find($request->id_barang);

            $inventori = Inventori::where('barang_id',$request->id_barang)
                ->where('tersedia',true)
                ->count();

            if($inventori < $barang->jumlah_barang){
                $inventori = new Inventori;
                $inventori->barang_id = $request->id_barang;
                $inventori->ruangan_id = $request->id_ruangan;
                $inventori->nama_inventori = $request->nama_inventori;
                $inventori->keadaan_barang = $request->keadaan_barang;

                $inventori->save();
                return redirect('/admin-inventori/inventori')->with('status', 'Data Inventori Berhasil Ditambahkan !');
            }else{
                return redirect()->back()->with('gagal', 'Data melebihi jumlah barang !');
            }

        }

        if(!empty($request->id_ruangan)){

            $inventori = Inventori::where('ruangan_id',$request->id_ruangan)
                ->where('tersedia',true)
                ->limit(1)
                ->count();

            if($inventori < 1){
                $inventori = new Inventori;
                $inventori->barang_id = $request->id_barang;
                $inventori->ruangan_id = $request->id_ruangan;
                $inventori->nama_inventori = $request->nama_inventori;
                $inventori->keadaan_barang = $request->keadaan_barang;

                $inventori->save();
                return redirect('/admin-inventori/inventori')->with('status', 'Data Inventori Berhasil Ditambahkan !');
            }else{
                return redirect()->back()->with('gagal', 'Data melebihi jumlah ruangan !');
            }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inventori = Inventori::find($id)->update([
            'keadaan_barang' => $request->keadaan_barang,
        ]);
        return redirect('/admin-inventori/inventori')->with('status', 'Data Inventori Berhasil Diubah !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inventori = Inventori::find($id);

        if(!empty($inventori->barang_id))
        {
            $barang = Barang::find($inventori->barang_id);
            $jumlah_barang = intval($barang->jumlah_barang - 1);

            if($jumlah_barang > 0){
                $barang->update([
                    'jumlah_barang' => $jumlah_barang
                ]);
            }
        }

        if(!empty($inventori->ruangan_id))
        {
            $ruangan = Ruangan::find($inventori->ruangan_id)->delete();
        }

        $inventori->update([
            'tersedia' => false
        ]);

        return redirect('/admin-inventori/inventori')->with('statusdel', 'Data Inventori Berhasil Dihapus!');
    }
}
