<?php

namespace App\Http\Controllers\AdminInventori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\Kategori;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = Barang::paginate(15);
        $kategori = Kategori::all();
        return view('admin-inventori.barang', compact('barang','kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'jumlah' => ['required','numeric'],
        ]);

        $barang = DB::table('tb_barang')
            ->where('nama_barang',$request->nama_barang)
            ->whereNull('deleted_at')
            ->first();

        if($barang){
            return redirect()->back()->with('statusdel', 'Nama barang sudah ada sebelumnya!');
        }

        if($request->jumlah <= 0){
            return redirect()->back()->with('statusdel', 'Jumlah Barang Tidak Boleh kosong!');
        }

        $barang = new Barang;
        $barang->nama_barang = $request->nama_barang;
        $barang->kategori_id = $request->id_kategori;
        $barang->jumlah_barang = $request->jumlah;
        $barang->keterangan = $request->keterangan;

        $barang->save();
        return redirect('/admin-inventori/barang')->with('status', 'Data Barang Berhasil Ditambahkan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'nama_barang' => ['required',Rule::unique('tb_barang')->ignore($id),],
        ]);

        Barang::where('id', $id)
        ->update([
            'nama_barang' => $request->nama_barang,
            'kategori_id' => $request->id_kategori,
            'jumlah_barang' => $request->jumlah,
            'keterangan' => $request->keterangan,
        ]);
        return redirect('/admin-inventori/barang')->with('status', 'Data Barang Berhasil Diubah !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barang::destroy($id);
        return redirect('/admin-inventori/barang')->with('statusdel', 'Data Barang Berhasil Dihapus!');
    }
}
