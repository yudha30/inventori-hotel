<?php

namespace App\Http\Controllers\AdminInventori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\Barang;
use App\Models\Permintaan;
use App\Models\DetailPermintaan;

class PermintaanController extends Controller
{

    public function permintaan()
    {
        $permintaan = Permintaan::all();
        return view('admin-inventori.permintaan.permintaan', compact('permintaan'));
    }

    public function update(Request $request, $id)
    {
        $data = Permintaan::find($id)->update([
            'status' => $request->status
        ]);
        return redirect()->back()->with('status', 'Data Permintaan Berhasil Diubah !');
    }

    public function destroy($id)
    {
        $delete = Permintaan::find($id)->delete();
        return redirect()->back()->with('statusdel', 'Data Permintaan Berhasil Dihapus!');
    }


    // detail
    public function index($id)
    {
        $supplier = Supplier::all();
        // $detail_permintaan = DetailPermintaan::where('permintaan_id',$id)->get();

        $permintaan = DB::table('tb_permintaan')
            ->select([
                'tb_permintaan.id',
                'tb_permintaan.status as status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_permintaan.user_id','=','tb_user.id');

        $detail_permintaan = DB::table('tb_detail_permintaan')
            ->select([
                'tb_detail_permintaan.*',
                'tb_barang.nama_barang',
                'tb_supplier.nama_supplier',
                'tb_permintaan.status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.nama_lengkap',
            ])
            ->leftJoin('tb_barang','tb_detail_permintaan.barang_id','=','tb_barang.id')
            ->leftJoin('tb_supplier','tb_detail_permintaan.supplier_id','=','tb_supplier.id')
            ->leftJoinSub($permintaan, 'tb_permintaan', function ($join) {
                $join->on('tb_detail_permintaan.permintaan_id', '=', 'tb_permintaan.id');
            })
            ->where('tb_detail_permintaan.permintaan_id',$id)
            ->whereNull('tb_detail_permintaan.deleted_at')
            ->orderByDesc('tb_detail_permintaan.updated_at')
            ->get();
        return view('admin-inventori.permintaan.detail_permintaan', compact('supplier','detail_permintaan'));
    }

    public function update_detail(Request $request, $id)
    {
        $detail_permintaan = DetailPermintaan::find($id);
        if($request->jumlah_permintaan > $detail_permintaan->acc_permintaan){
            return redirect()->back()->with('statusdel', 'Permintaan melebihi jumlah!');
        }

        $detail_permintaan->update([
            'supplier_id' => $request->id_supplier,
            'status' => $request->status,
            'jumlah_permintaan' => $request->jumlah_permintaan
        ]);

        if($request->status == 'Terpenuhi'){
            $barang = Barang::find($detail_permintaan->barang_id);
            $tambah_barang = (int)$barang->jumlah_barang+$detail_permintaan->jumlah_permintaan;
            $barang->update([
                'jumlah_barang' => $tambah_barang
            ]);
        }
        return redirect()->back()->with('status', 'Data Permintaan Berhasil Diubah !');
    }

    public function destroy_detail($id)
    {
        $detail_permintaan = DetailPermintaan::where('id', $id)->get();
        $jumlah_detail_permintaan = DetailPermintaan::where('permintaan_id', $detail_permintaan[0]->permintaan_id)->get();
        $total_detail_permintaan = count($jumlah_detail_permintaan);

        if ($total_detail_permintaan != 1) {
            DetailPermintaan::destroy($id);
            return redirect()->back()->with('statusdel', 'Data Permintaan Berhasil Dihapus!');
        }else {
            DetailPermintaan::destroy($id);
            Permintaan::destroy($detail_permintaan[0]->permintaan_id);
            return redirect()->back()->with('statusdel', 'Data Permintaan Berhasil Dihapus!');
        }
    }
}
