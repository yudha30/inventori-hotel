<?php

namespace App\Http\Controllers\AdminInventori;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Ruangan;

class RuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ruangan = Ruangan::paginate(15);
        return view('admin-inventori.ruangan', compact('ruangan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ruangan = DB::table('tb_ruangan')
            ->where('nama_ruangan',$request->nama_ruangan)
            ->whereNull('deleted_at')
            ->first();
        if($ruangan){
            return redirect()->back()->with('statusdel', 'Nama ruangan sudah ada sebelumnya!');
        }else{
            $ruangan = new Ruangan;
            $ruangan->nama_ruangan = $request->nama_ruangan;

            $ruangan->save();
            return redirect('/admin-inventori/ruangan')->with('status', 'Data Ruangan Berhasil Ditambahkan !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Ruangan::where('id', $id)
            ->update([
                'nama_ruangan' => $request->nama_ruangan,
            ]);
            return redirect('/admin-inventori/ruangan')->with('status', 'Data Ruangan Berhasil Diubah !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ruangan::destroy($id);
        return redirect('/admin-inventori/ruangan')->with('statusdel', 'Data Ruangan Berhasil Dihapus!');
    }
}
