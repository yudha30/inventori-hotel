<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Perbaikan;
use App\Models\DetailPerbaikan;
use App\Models\Inventori;
use DB;

class PerbaikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tb_inventori = Inventori::all();
        $tb_perbaikan = Perbaikan::paginate(15);
        // $detail_perbaikan = DetailPerbaikan::all();

        $cek_inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $detail = DB::table('tb_detail_perbaikan')
            ->select([
                'tb_detail_perbaikan.id',
                'tb_detail_perbaikan.perbaikan_id',
                'tb_detail_perbaikan.status as status_detail',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
            ])
            ->leftJoinSub($cek_inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_perbaikan.inventori_id', '=', 'tb_inventori.id');
            });
        // dd($detail->get());
        $status = DB::table('tb_perbaikan')
            ->select([
                'tb_detail_perbaikan.nama_barang',
                'tb_detail_perbaikan.nama_ruangan',
                'tb_detail_perbaikan.status_detail',
            ])
            ->leftJoinSub($detail, 'tb_detail_perbaikan', function ($join) {
                $join->on('tb_perbaikan.id', '=', 'tb_detail_perbaikan.perbaikan_id');
            })
            ->where([
                'tb_detail_perbaikan.status_detail' => 'Tidak Bisa Diperbaiki',
                'tb_perbaikan.status' => 'Acc Admin'
            ])
            ->whereNull('tb_perbaikan.deleted_at')
            ->get();
        // dd($status);

        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan'
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $perbaikan = DB::table('tb_perbaikan')
            ->select([
                'tb_perbaikan.id',
                'tb_perbaikan.status as status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.user_id',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_perbaikan.user_id','=','tb_user.id');

        $detail_perbaikan = DB::table('tb_detail_perbaikan')
            ->select([
                'tb_detail_perbaikan.*',
                'tb_perbaikan.status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_perbaikan.user_id',
            ])
            ->leftJoinSub($perbaikan, 'tb_perbaikan', function ($join) {
                $join->on('tb_detail_perbaikan.perbaikan_id', '=', 'tb_perbaikan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_perbaikan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereNull('tb_detail_perbaikan.deleted_at')
            ->orderByDesc('tb_detail_perbaikan.updated_at')
            ->paginate(15);
        return view('manager.perbaikan', compact('tb_inventori','tb_perbaikan','detail_perbaikan','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Perbaikan::where('id', $id)
        ->update([
            'status' => $request->status,
        ]);
        return redirect('/manager/transaksi/transaksi-perbaikan')->with('status', 'Data Perbaikan Berhasil Diubah !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Perbaikan::destroy($id);
        DetailPerbaikan::where('perbaikan_id', $id)->delete();
        return redirect('/manager/transaksi/transaksi-permintaan')->with('statusdel', 'Data Perbaikan Berhasil Dihapus!');
    }
}
