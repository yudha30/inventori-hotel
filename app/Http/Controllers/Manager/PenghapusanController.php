<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Penghapusan;
use App\Models\DetailPenghapusan;
use App\Models\Inventori;
use App\Models\Barang;
use App\Models\Ruangan;

class PenghapusanController extends Controller
{

    public function index()
    {
        $tb_inventori = Inventori::where('keadaan_barang','Tidak Bisa Diperbaiki')
            ->orWhere('keadaan_barang','Rusak')
            ->get();
        $cek_pengajuan = DetailPenghapusan::where('keterangan','Pengajuan Penghapusan')->get();

        $penghapusan = DB::table('tb_penghapusan')
            ->select([
                'tb_penghapusan.id',
                'tb_penghapusan.user_id',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_penghapusan.user_id','=','tb_user.id');

        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $detail_penghapusan = DB::table('tb_detail_penghapusan')
            ->select([
                'tb_detail_penghapusan.*',
                'tb_penghapusan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_penghapusan.user_id',

            ])
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_penghapusan.inventori_id', '=', 'tb_inventori.id');
            })
            ->joinSub($penghapusan, 'tb_penghapusan', function ($join) {
                $join->on('tb_detail_penghapusan.penghapusan_id', '=', 'tb_penghapusan.id');
            })
            ->whereNull('tb_detail_penghapusan.deleted_at')
            ->paginate(15);

        return view('manager.penghapusan', compact('tb_inventori','detail_penghapusan','cek_pengajuan'));
    }

    public function store(Request $request)
    {
        $penghapusan = new Penghapusan;
        $penghapusan->user_id = $request->id_user;
        $penghapusan->tgl_penghapusan = now();

        $penghapusan->save();

        foreach ($request->id_inventori as $key => $data){
            $detail_penghapusan = new DetailPenghapusan;
            $detail_penghapusan->penghapusan_id = $penghapusan->id;
            $detail_penghapusan->inventori_id = $data;
            $detail_penghapusan->keterangan = "Acc Manager";
            $detail_penghapusan->save();

            // $inventory = Inventori::find($data);

            // if(!empty($inventori->barang_id)){
            //     $barang = Barang::find($inventori->barang_id);
            //     $total_barang = intval($barang->jumlah_barang - 1);
            //     if($total_barang > 0){
            //         $barang->update([
            //             'jumlah_barang' => $total_barang
            //         ]);
            //     }
            // }
        };

        return redirect('/manager/transaksi/transaksi-penghapusan')->with('status', 'Data Penghapusan Berhasil Ditambahkan !');
    }

    public function update(Request $request, $id)
    {
        if(!empty($request->status)){
            DetailPenghapusan::where('id', $id)
            ->update([
                'inventori_id' => $request->id_inventori,
                'keterangan' => $request->status
            ]);

            // $data = DetailPenghapusan::find($id);

            // $inventori = Inventori::find($data->inventori_id);
            // if(!empty($inventori->barang_id)){
            //     $barang = Barang::find($inventori->barang_id);
            //     $total_barang = intval($barang->jumlah_barang - 1);

            //     if($total_barang > 0){
            //         $barang->update([
            //             'jumlah_barang' => $total_barang
            //         ]);
            //     }
            // }
        }

        return redirect('/manager/transaksi/transaksi-penghapusan')->with('status', 'Data Penghapusan Berhasil Diubah !');
    }

    public function destroy($id)
    {
        $detail_penghapusan = DetailPenghapusan::where('id', $id)->get();
        $jumlah_detail_penghapusan = DetailPenghapusan::where('penghapusan_id', $detail_penghapusan[0]->penghapusan_id)->get();
        $total_detail_penghapusan = count($jumlah_detail_penghapusan);

        if ($total_detail_penghapusan != 1) {
            DetailPenghapusan::destroy($id);
            return redirect('/manager/transaksi/transaksi-penghapusan')->with('statusdel', 'Data Penghapusan Berhasil Dihapus!');
        }else {
            Detailpenghapusan::destroy($id);
            penghapusan::destroy($detail_penghapusan[0]->penghapusan_id);
            return redirect('/manager/transaksi/transaksi-penghapusan')->with('statusdel', 'Data Penghapusan Berhasil Dihapus!');
        }
    }
}
