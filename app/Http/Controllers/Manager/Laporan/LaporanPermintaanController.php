<?php

namespace App\Http\Controllers\Manager\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\DetailPermintaan;

use PDF;

class LaporanPermintaanController extends Controller
{
    public function index()
    {

        // $data = DetailPermintaan::all();

        $permintaan = DB::table('tb_permintaan')
            ->select([
                'tb_permintaan.id',
                'tb_permintaan.status as status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_permintaan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_permintaan')
            ->select([
                'tb_detail_permintaan.*',
                'tb_barang.nama_barang',
                'tb_supplier.nama_supplier',
                'tb_permintaan.status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.nama_lengkap',
            ])
            ->leftJoin('tb_barang','tb_detail_permintaan.barang_id','=','tb_barang.id')
            ->leftJoin('tb_supplier','tb_detail_permintaan.supplier_id','=','tb_supplier.id')
            ->leftJoinSub($permintaan, 'tb_permintaan', function ($join) {
                $join->on('tb_detail_permintaan.permintaan_id', '=', 'tb_permintaan.id');
            })
            ->whereNull('tb_detail_permintaan.deleted_at')
            ->orderByDesc('tb_detail_permintaan.updated_at')
            ->get();

        return view('manager.laporan.laporan-permintaan',compact('data'));
    }

    public function cari(Request $request)
    {
        $tanggal_awal = date('Y-m-d', strtotime($request->tanggal_awal));
        $tanggal_akhir = date('Y-m-d', strtotime($request->tanggal_akhir));


        $permintaan = DB::table('tb_permintaan')
            ->select([
                'tb_permintaan.id',
                'tb_permintaan.status as status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_permintaan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_permintaan')
            ->select([
                'tb_detail_permintaan.*',
                'tb_barang.nama_barang',
                'tb_supplier.nama_supplier',
                'tb_permintaan.status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.nama_lengkap',
            ])
            ->leftJoin('tb_barang','tb_detail_permintaan.barang_id','=','tb_barang.id')
            ->leftJoin('tb_supplier','tb_detail_permintaan.supplier_id','=','tb_supplier.id')
            ->leftJoinSub($permintaan, 'tb_permintaan', function ($join) {
                $join->on('tb_detail_permintaan.permintaan_id', '=', 'tb_permintaan.id');
            })
            ->whereBetween('tb_permintaan.tgl_permintaan', [$tanggal_awal, $tanggal_akhir])
            ->whereNull('tb_detail_permintaan.deleted_at')
            ->orderByDesc('tb_detail_permintaan.updated_at')
            ->get();

        return view('manager.laporan.laporan-permintaan',compact('data','tanggal_awal','tanggal_akhir'));
    }

    public function cetak($tanggal_awal, $tanggal_akhir)
    {
        if($tanggal_awal==0 && $tanggal_akhir == 0){
            $permintaan = DB::table('tb_permintaan')
            ->select([
                'tb_permintaan.id',
                'tb_permintaan.status as status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_permintaan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_permintaan')
            ->select([
                'tb_detail_permintaan.*',
                'tb_barang.nama_barang',
                'tb_supplier.nama_supplier',
                'tb_permintaan.status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.nama_lengkap',
            ])
            ->leftJoin('tb_barang','tb_detail_permintaan.barang_id','=','tb_barang.id')
            ->leftJoin('tb_supplier','tb_detail_permintaan.supplier_id','=','tb_supplier.id')
            ->leftJoinSub($permintaan, 'tb_permintaan', function ($join) {
                $join->on('tb_detail_permintaan.permintaan_id', '=', 'tb_permintaan.id');
            })
            ->whereNull('tb_detail_permintaan.deleted_at')
            ->orderByDesc('tb_detail_permintaan.updated_at')
            ->get();
        }else{
            $permintaan = DB::table('tb_permintaan')
            ->select([
                'tb_permintaan.id',
                'tb_permintaan.status as status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_permintaan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_permintaan')
            ->select([
                'tb_detail_permintaan.*',
                'tb_barang.nama_barang',
                'tb_supplier.nama_supplier',
                'tb_permintaan.status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.nama_lengkap',
            ])
            ->leftJoin('tb_barang','tb_detail_permintaan.barang_id','=','tb_barang.id')
            ->leftJoin('tb_supplier','tb_detail_permintaan.supplier_id','=','tb_supplier.id')
            ->leftJoinSub($permintaan, 'tb_permintaan', function ($join) {
                $join->on('tb_detail_permintaan.permintaan_id', '=', 'tb_permintaan.id');
            })
            ->whereBetween('tb_permintaan.tgl_permintaan', [$tanggal_awal, $tanggal_akhir])
            ->whereNull('tb_detail_permintaan.deleted_at')
            ->orderByDesc('tb_detail_permintaan.updated_at')
            ->get();
        }

        $pdf = PDF::loadView('manager.print.permintaan', compact('data'));
        return $pdf->download('Laporan Permintaan.pdf');

    }
}
