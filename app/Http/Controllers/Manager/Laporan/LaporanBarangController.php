<?php

namespace App\Http\Controllers\Manager\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Barang;

use PDF;

class LaporanBarangController extends Controller
{
    public function index()
    {

        $data = Barang::all();

        return view('manager.laporan.laporan-barang',compact('data'));
    }

    public function cetak()
    {
        $data = Barang::all();

        $pdf = PDF::loadView('manager.print.barang', compact('data'));
        return $pdf->download('Laporan Barang.pdf');

    }
}
