<?php

namespace App\Http\Controllers\Manager\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\DetailPengecekan;

use PDF;

class LaporanPengecekanController extends Controller
{
    public function index()
    {

        // $data = DetailPengecekan::all();
        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $pengecekan = DB::table('tb_pengecekan')
            ->select([
                'tb_pengecekan.id',
                'tb_pengecekan.tgl_pengecekan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_pengecekan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_pengecekan')
            ->select([
                'tb_detail_pengecekan.*',
                'tb_pengecekan.tgl_pengecekan',
                'tb_pengecekan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($pengecekan, 'tb_pengecekan', function ($join) {
                $join->on('tb_detail_pengecekan.pengecekan_id', '=', 'tb_pengecekan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_pengecekan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereNull('tb_detail_pengecekan.deleted_at')
            ->orderByDesc('tb_detail_pengecekan.updated_at')
            ->get();

        return view('manager.laporan.laporan-pengecekan',compact('data'));
    }

    public function cari(Request $request)
    {
        $tanggal_awal = date('Y-m-d', strtotime($request->tanggal_awal));
        $tanggal_akhir = date('Y-m-d', strtotime($request->tanggal_akhir));

        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $pengecekan = DB::table('tb_pengecekan')
            ->select([
                'tb_pengecekan.id',
                'tb_pengecekan.tgl_pengecekan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_pengecekan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_pengecekan')
            ->select([
                'tb_detail_pengecekan.*',
                'tb_pengecekan.tgl_pengecekan',
                'tb_pengecekan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($pengecekan, 'tb_pengecekan', function ($join) {
                $join->on('tb_detail_pengecekan.pengecekan_id', '=', 'tb_pengecekan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_pengecekan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereBetween('tb_pengecekan.tgl_pengecekan', [$tanggal_awal, $tanggal_akhir])
            ->whereNull('tb_detail_pengecekan.deleted_at')
            ->orderByDesc('tb_detail_pengecekan.updated_at')
            ->get();

        return view('manager.laporan.laporan-pengecekan',compact('data','tanggal_awal','tanggal_akhir'));
    }

    public function cetak($tanggal_awal, $tanggal_akhir)
    {
        if($tanggal_awal==0 && $tanggal_akhir == 0){
            $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $pengecekan = DB::table('tb_pengecekan')
            ->select([
                'tb_pengecekan.id',
                'tb_pengecekan.tgl_pengecekan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_pengecekan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_pengecekan')
            ->select([
                'tb_detail_pengecekan.*',
                'tb_pengecekan.tgl_pengecekan',
                'tb_pengecekan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($pengecekan, 'tb_pengecekan', function ($join) {
                $join->on('tb_detail_pengecekan.pengecekan_id', '=', 'tb_pengecekan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_pengecekan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereNull('tb_detail_pengecekan.deleted_at')
            ->orderByDesc('tb_detail_pengecekan.updated_at')
            ->get();
        }else{
            $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $pengecekan = DB::table('tb_pengecekan')
            ->select([
                'tb_pengecekan.id',
                'tb_pengecekan.tgl_pengecekan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_pengecekan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_pengecekan')
            ->select([
                'tb_detail_pengecekan.*',
                'tb_pengecekan.tgl_pengecekan',
                'tb_pengecekan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($pengecekan, 'tb_pengecekan', function ($join) {
                $join->on('tb_detail_pengecekan.pengecekan_id', '=', 'tb_pengecekan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_pengecekan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereBetween('tb_pengecekan.tgl_pengecekan', [$tanggal_awal, $tanggal_akhir])
            ->whereNull('tb_detail_pengecekan.deleted_at')
            ->orderByDesc('tb_detail_pengecekan.updated_at')
            ->get();
        }

        $pdf = PDF::loadView('manager.print.pengecekan', compact('data'));
        return $pdf->download('Laporan pengecekan.pdf');

    }
}
