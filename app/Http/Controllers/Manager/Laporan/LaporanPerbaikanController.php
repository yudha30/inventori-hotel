<?php

namespace App\Http\Controllers\Manager\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\DetailPerbaikan;

use PDF;

class LaporanPerbaikanController extends Controller
{
    public function index()
    {

        // $data = DetailPerbaikan::all();

        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $perbaikan = DB::table('tb_perbaikan')
            ->select([
                'tb_perbaikan.id',
                'tb_perbaikan.status as status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_perbaikan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_perbaikan')
            ->select([
                'tb_detail_perbaikan.*',
                'tb_perbaikan.status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($perbaikan, 'tb_perbaikan', function ($join) {
                $join->on('tb_detail_perbaikan.perbaikan_id', '=', 'tb_perbaikan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_perbaikan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereNull('tb_detail_perbaikan.deleted_at')
            ->orderByDesc('tb_detail_perbaikan.updated_at')
            ->get();

        return view('manager.laporan.laporan-perbaikan',compact('data'));
    }

    public function cari(Request $request)
    {
        $tanggal_awal = date('Y-m-d', strtotime($request->tanggal_awal));
        $tanggal_akhir = date('Y-m-d', strtotime($request->tanggal_akhir));
        // $data = DetailPerbaikan::
        //     leftJoin('tb_perbaikan','tb_detail_perbaikan.perbaikan_id','=','tb_perbaikan.id')
        //     ->whereBetween('tb_perbaikan.tgl_perbaikan', [$tanggal_awal, $tanggal_akhir])
        //     ->get();
        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $perbaikan = DB::table('tb_perbaikan')
            ->select([
                'tb_perbaikan.id',
                'tb_perbaikan.status as status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_perbaikan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_perbaikan')
            ->select([
                'tb_detail_perbaikan.*',
                'tb_perbaikan.status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($perbaikan, 'tb_perbaikan', function ($join) {
                $join->on('tb_detail_perbaikan.perbaikan_id', '=', 'tb_perbaikan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_perbaikan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereBetween('tb_perbaikan.tgl_perbaikan', [$tanggal_awal, $tanggal_akhir])
            ->whereNull('tb_detail_perbaikan.deleted_at')
            ->orderByDesc('tb_detail_perbaikan.updated_at')
            ->get();

        return view('manager.laporan.laporan-perbaikan',compact('data','tanggal_awal','tanggal_akhir'));
    }

    public function cetak($tanggal_awal, $tanggal_akhir)
    {
        if($tanggal_awal==0 && $tanggal_akhir == 0){
            // $data = DetailPerbaikan::all();
            $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $perbaikan = DB::table('tb_perbaikan')
            ->select([
                'tb_perbaikan.id',
                'tb_perbaikan.status as status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_perbaikan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_perbaikan')
            ->select([
                'tb_detail_perbaikan.*',
                'tb_perbaikan.status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($perbaikan, 'tb_perbaikan', function ($join) {
                $join->on('tb_detail_perbaikan.perbaikan_id', '=', 'tb_perbaikan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_perbaikan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereNull('tb_detail_perbaikan.deleted_at')
            ->orderByDesc('tb_detail_perbaikan.updated_at')
            ->get();
        }else{
            // $data = DetailPerbaikan::
            //     leftJoin('tb_perbaikan','tb_detail_perbaikan.perbaikan_id','=','tb_perbaikan.id')
            //     ->whereBetween('tb_perbaikan.tgl_perbaikan', [$tanggal_awal, $tanggal_akhir])
            //     ->get();
            $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $perbaikan = DB::table('tb_perbaikan')
            ->select([
                'tb_perbaikan.id',
                'tb_perbaikan.status as status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_perbaikan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_perbaikan')
            ->select([
                'tb_detail_perbaikan.*',
                'tb_perbaikan.status_perbaikan',
                'tb_perbaikan.tgl_perbaikan',
                'tb_perbaikan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($perbaikan, 'tb_perbaikan', function ($join) {
                $join->on('tb_detail_perbaikan.perbaikan_id', '=', 'tb_perbaikan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_perbaikan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereBetween('tb_perbaikan.tgl_perbaikan', [$tanggal_awal, $tanggal_akhir])
            ->whereNull('tb_detail_perbaikan.deleted_at')
            ->orderByDesc('tb_detail_perbaikan.updated_at')
            ->get();
        }

        $pdf = PDF::loadView('manager.print.perbaikan', compact('data'));
        return $pdf->download('Laporan perbaikan.pdf');

    }
}
