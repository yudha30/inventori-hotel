<?php

namespace App\Http\Controllers\Manager\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ruangan;

use PDF;
class LaporanRuanganController extends Controller
{
    public function index()
    {

        $data = Ruangan::all();

        return view('manager.laporan.laporan-ruangan',compact('data'));
    }

    public function cetak()
    {
        $data = Ruangan::all();

        $pdf = PDF::loadView('manager.print.ruangan', compact('data'));
        return $pdf->download('Laporan Ruangan.pdf');

    }
}
