<?php

namespace App\Http\Controllers\Manager\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use PDF;

class LaporanInventoriController extends Controller
{
    public function index()
    {

        $data = DB::table('tb_inventori')
            ->select([
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang'
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id')
            ->where('tb_inventori.tersedia',true)
            ->get();

        return view('manager.laporan.laporan-inventori',compact('data'));
    }

    public function cetak()
    {
        $data = DB::table('tb_inventori')
            ->select([
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang'
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id')
            ->where('tb_inventori.tersedia',true)
            ->get();

        $pdf = PDF::loadView('manager.print.inventori', compact('data'));
        return $pdf->download('Laporan Inventori.pdf');

    }
}
