<?php

namespace App\Http\Controllers\Manager\Laporan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\DetailPenghapusan;

use PDF;

class LaporanPenghapusanController extends Controller
{
    public function index()
    {

        // $data = DetailPenghapusan::all();
        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $penghapusan = DB::table('tb_penghapusan')
            ->select([
                'tb_penghapusan.id',
                'tb_penghapusan.tgl_penghapusan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_penghapusan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_penghapusan')
            ->select([
                'tb_detail_penghapusan.*',
                'tb_penghapusan.tgl_penghapusan',
                'tb_penghapusan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($penghapusan, 'tb_penghapusan', function ($join) {
                $join->on('tb_detail_penghapusan.penghapusan_id', '=', 'tb_penghapusan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_penghapusan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereNull('tb_detail_penghapusan.deleted_at')
            ->orderByDesc('tb_detail_penghapusan.updated_at')
            ->get();

        return view('manager.laporan.laporan-penghapusan',compact('data'));
    }

    public function cari(Request $request)
    {
        $tanggal_awal = date('Y-m-d', strtotime($request->tanggal_awal));
        $tanggal_akhir = date('Y-m-d', strtotime($request->tanggal_akhir));
        // $data = DetailPenghapusan::
        //     leftJoin('tb_penghapusan','tb_detail_penghapusan.penghapusan_id','=','tb_penghapusan.id')
        //     ->whereBetween('tb_penghapusan.tgl_penghapusan', [$tanggal_awal, $tanggal_akhir])
        //     ->get();
        $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $penghapusan = DB::table('tb_penghapusan')
            ->select([
                'tb_penghapusan.id',
                'tb_penghapusan.tgl_penghapusan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_penghapusan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_penghapusan')
            ->select([
                'tb_detail_penghapusan.*',
                'tb_penghapusan.tgl_penghapusan',
                'tb_penghapusan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($penghapusan, 'tb_penghapusan', function ($join) {
                $join->on('tb_detail_penghapusan.penghapusan_id', '=', 'tb_penghapusan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_penghapusan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereBetween('tb_penghapusan.tgl_penghapusan', [$tanggal_awal, $tanggal_akhir])
            ->whereNull('tb_detail_penghapusan.deleted_at')
            ->orderByDesc('tb_detail_penghapusan.updated_at')
            ->get();

        return view('manager.laporan.laporan-penghapusan',compact('data','tanggal_awal','tanggal_akhir'));
    }

    public function cetak($tanggal_awal, $tanggal_akhir)
    {
        if($tanggal_awal==0 && $tanggal_akhir == 0){
            $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $penghapusan = DB::table('tb_penghapusan')
            ->select([
                'tb_penghapusan.id',
                'tb_penghapusan.tgl_penghapusan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_penghapusan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_penghapusan')
            ->select([
                'tb_detail_penghapusan.*',
                'tb_penghapusan.tgl_penghapusan',
                'tb_penghapusan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($penghapusan, 'tb_penghapusan', function ($join) {
                $join->on('tb_detail_penghapusan.penghapusan_id', '=', 'tb_penghapusan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_penghapusan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereNull('tb_detail_penghapusan.deleted_at')
            ->orderByDesc('tb_detail_penghapusan.updated_at')
            ->get();
        }else{
            $inventori = DB::table('tb_inventori')
            ->select([
                'tb_inventori.id',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_barang.nama_barang',
                'tb_ruangan.nama_ruangan',
            ])
            ->leftJoin('tb_barang','tb_inventori.barang_id','=','tb_barang.id')
            ->leftJoin('tb_ruangan','tb_inventori.ruangan_id','=','tb_ruangan.id');

        $penghapusan = DB::table('tb_penghapusan')
            ->select([
                'tb_penghapusan.id',
                'tb_penghapusan.tgl_penghapusan',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_penghapusan.user_id','=','tb_user.id');

        $data = DB::table('tb_detail_penghapusan')
            ->select([
                'tb_detail_penghapusan.*',
                'tb_penghapusan.tgl_penghapusan',
                'tb_penghapusan.nama_lengkap',
                'tb_inventori.nama_barang',
                'tb_inventori.nama_ruangan',
                'tb_inventori.nama_inventori',
                'tb_inventori.keadaan_barang',
                'tb_inventori.barang_id',
                'tb_inventori.ruangan_id',
            ])
            ->leftJoinSub($penghapusan, 'tb_penghapusan', function ($join) {
                $join->on('tb_detail_penghapusan.penghapusan_id', '=', 'tb_penghapusan.id');
            })
            ->leftJoinSub($inventori, 'tb_inventori', function ($join) {
                $join->on('tb_detail_penghapusan.inventori_id', '=', 'tb_inventori.id');
            })
            ->whereBetween('tb_penghapusan.tgl_penghapusan', [$tanggal_awal, $tanggal_akhir])
            ->whereNull('tb_detail_penghapusan.deleted_at')
            ->orderByDesc('tb_detail_penghapusan.updated_at')
            ->get();
        }

        $pdf = PDF::loadView('manager.print.penghapusan', compact('data'));
        return $pdf->download('Laporan penghapusan.pdf');

    }
}
