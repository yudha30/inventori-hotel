<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Permintaan;
use App\Models\Perbaikan;
use App\Models\Penghapusan;

class DashboardController extends Controller
{
    public function index()
    {
        $perbaikan = Perbaikan::all()->count();
        $permintaan = Permintaan::all()->count();
        $penghapusan = Penghapusan::all()->count();
        return view('manager.dashboard',compact('perbaikan','permintaan','penghapusan'));
    }
}
