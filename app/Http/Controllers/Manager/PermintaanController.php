<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;
use App\Models\Permintaan;
use App\Models\DetailPermintaan;
use DB;

class PermintaanController extends Controller
{
    public function index()
    {
        $supplier = Supplier::all();
        $permintaan = Permintaan::orderBy('tgl_permintaan','desc')->paginate(15);

        return view('manager.permintaan', compact('supplier','permintaan'));
    }

    public function update(Request $request, $id)
    {
        $permintaan = Permintaan::find($id);
        $permintaan->update([
            'status' => $request->status,
        ]);
        return redirect('/manager/transaksi/transaksi-permintaan')->with('status', 'Data Permintaan Berhasil Diubah !');
    }

    public function update_detail(Request $request, $id)
    {
        $update = DetailPermintaan::find($id);

        if($update->jumlah_permintaan != $request->jumlah_permintaan){

            $update->update([
                'jumlah_permintaan' => $request->jumlah_permintaan,
                'status_perubahan_permintaan' => 1,
                'acc_permintaan' => $request->jumlah_permintaan,
            ]);
        }
        else{
            $update->update([
                'acc_permintaan' => $request->jumlah_permintaan,
            ]);
        }

        return redirect()->back()->with('status', 'Data Permintaan Berhasil Diubah !');
    }

    public function destroy($id)
    {
        Permintaan::destroy($id);
        DetailPermintaan::where('permintaan_id', $id)->delete();
        return redirect('/manager/transaksi/transaksi-permintaan')->with('statusdel', 'Data Permintaan Berhasil Dihapus!');
    }

    public function detail($id)
    {
        $permintaan = DB::table('tb_permintaan')
            ->select([
                'tb_permintaan.id',
                'tb_permintaan.status as status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.user_id',
                'tb_user.nama_lengkap',
            ])
            ->leftJoin('tb_user','tb_permintaan.user_id','=','tb_user.id');

        $detail_permintaan = DB::table('tb_detail_permintaan')
            ->select([
                'tb_detail_permintaan.*',
                'tb_barang.nama_barang',
                'tb_supplier.nama_supplier',
                'tb_permintaan.status_permintaan',
                'tb_permintaan.tgl_permintaan',
                'tb_permintaan.nama_lengkap',
                'tb_permintaan.user_id',
            ])
            ->leftJoin('tb_barang','tb_detail_permintaan.barang_id','=','tb_barang.id')
            ->leftJoin('tb_supplier','tb_detail_permintaan.supplier_id','=','tb_supplier.id')
            ->leftJoinSub($permintaan, 'tb_permintaan', function ($join) {
                $join->on('tb_detail_permintaan.permintaan_id', '=', 'tb_permintaan.id');
            })
            ->whereNull('tb_detail_permintaan.deleted_at')
            ->orderByDesc('tb_detail_permintaan.updated_at')
            ->paginate(15);
        return view('manager.detail_permintaan', compact('detail_permintaan'));
    }

    public function destroy_detail($id)
    {
        $detail = DetailPermintaan::find($id);
        $permintaan_id=$detail->permintaan_id;
        $check = DetailPermintaan::where('permintaan_id',$detail->permintaan_id)->count();

        if($check<=1){
            $delete_detail = DetailPermintaan::destroy($id);
            $delete_permintaan = Permintaan::destroy($permintaan_id);
            return redirect('manager/transaksi/transaksi-permintaan')->with('statusdel', 'Data Permintaan Berhasil Dihapus!');
        }
        else{
            $delete_detail = DetailPermintaan::destroy($id);
            return redirect()->back()->with('statusdel', 'Data Permintaan Berhasil Dihapus!');
        }
    }
}
