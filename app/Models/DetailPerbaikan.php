<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPerbaikan extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'tb_detail_perbaikan';

    protected $fillable = [
        'perbaikan_id',
        'inventori_id',
        'keterangan',
        'status'
    ];

    protected $dates = ['deleted_at'];

    public function perbaikan()
    {
        return $this->belongsTo('App\Models\Perbaikan', 'perbaikan_id');
    }

    public function inventori()
    {
        return $this->belongsTo('App\Models\Inventori', 'inventori_id');
    }
}
