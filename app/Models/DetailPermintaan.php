<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPermintaan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'tb_detail_permintaan';
    protected $fillable = [
        'permintaan_id',
        'supplier_id',
        'barang_id',
        'jumlah_permintaan',
        'status',
        'status_perubahan_permintaan',
        'acc_permintaan',
    ];

    protected $dates = ['deleted_at'];

    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id');
    }

    public function permintaan()
    {
        return $this->belongsTo('App\Models\Permintaan', 'permintaan_id');
    }

    public function barang()
    {
        return $this->belongsTo('App\Models\Barang', 'barang_id');
    }
}
