<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPenghapusan extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'tb_detail_penghapusan';

    protected $fillable = [
        'penghapusan_id',
        'inventori_id',
        'keterangan',
    ];

    protected $dates = ['deleted_at'];

    public function penghapusan()
    {
        return $this->belongsTo('App\Models\Penghapusan', 'penghapusan_id');
    }

    public function inventori()
    {
        return $this->belongsTo('App\Models\Inventori', 'inventori_id');
    }
}
