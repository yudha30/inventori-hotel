<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Penghapusan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'tb_penghapusan';
    protected $fillable = [
        'user_id',
        'tgl_penghapusan',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function detail_penghapusan()
    {
        return $this->hasMany('App\Models\DetailPenghapusan');
    }
}
