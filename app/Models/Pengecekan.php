<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengecekan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'tb_pengecekan';
    protected $fillable = [
        'user_id',
        'tgl_pengecekan',
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function detail_pengecekan()
    {
        return $this->hasMany('App\Models\DetailPengecekan');
    }
}
