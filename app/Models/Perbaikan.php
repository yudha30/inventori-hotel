<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perbaikan extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'tb_perbaikan';
    protected $fillable = [
        'user_id',
        'tgl_perbaikan',
        'status'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function detail_perbaikan()
    {
        return $this->hasMany('App\Models\DetailPerbaikan');
    }
}
