<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'tb_supplier';
    protected $fillable = [
        'nama_supplier',
        'alamat_supplier',
        'no_hp',
    ];

    protected $dates = ['deleted_at'];

    public function detail_permintaan()
    {
        return $this->hasMany('App\Model\DetailPermintaan');
    }
}
