<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventori extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'tb_inventori';
    protected $fillable = [
        'barang_id',
        'ruangan_id',
        'nama_inventori',
        'keadaan_barang',
        'tersedia'
    ];
    public function barang()
    {
        return $this->belongsTo('App\Models\Barang');
    }
    public function ruangan()
    {
        return $this->belongsTo('App\Models\Ruangan');
    }
}
