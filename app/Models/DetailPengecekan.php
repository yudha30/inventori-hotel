<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetailPengecekan extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'tb_detail_pengecekan';

    protected $fillable = [
        'pengecekan_id',
        'inventori_id',
        'keterangan',
    ];

    protected $dates = ['deleted_at'];

    public function pengecekan()
    {
        return $this->belongsTo('App\Models\Pengecekan', 'pengecekan_id');
    }

    public function inventori()
    {
        return $this->belongsTo('App\Models\Inventori', 'inventori_id');
    }
}
