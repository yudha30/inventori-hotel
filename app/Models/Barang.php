<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barang extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'tb_barang';
    protected $fillable = [
        'kategori_id',
        'nama_barang',
        'jumlah_barang',
        'keterangan'
    ];

    protected $dates = ['deleted_at'];

    public function kategori()
    {
        return $this->belongsTo('App\Models\Kategori', 'kategori_id');
    }

    public function inventori()
    {
        return $this->hasMany('App\Models\Inventori');
    }

    public function detail_permintaan()
    {
        return $this->hasMany('App\Model\DetailPermintaan');
    }
}
