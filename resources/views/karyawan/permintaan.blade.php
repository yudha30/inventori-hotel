@extends('layouts.index')
@push('style')
    <script src="{{ asset('template/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('template/assets/vendor/toastr/toastr.min.js') }}"></script>
@endpush
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Permintaan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Permintaan
                </button>
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif
                @if(count($status)>0)
                    @foreach ($status as $key => $value)
                        <script>
                            toastr.warning('Permintaan {{$value->nama_barang}} mengalami perubahan.', 'Peringatan');
                        </script>
                    @endforeach
                @endif

                <!-- Modal + permintaan -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Permintaan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/karyawan/transaksi/create-detail-permintaan"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" name="id_user"
                                            value="{{Auth::guard('karyawan')->user()->id}}">
                                    </div>
                                    <div class="permintaan">
                                        <div class="isi">
                                            <div class="form-group">
                                                <label for="barang">Barang</label>
                                                <select class="col-8 form-control" name="id_barang[]" required>
                                                    <option value="">Pilih Barang</option>
                                                    @foreach ($barang as $brg)
                                                    <option value="{{$brg->id}}">{{$brg->nama_barang}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="jumlah">Jumlah</label>
                                                <input type="number" class="form-control col-8 "
                                                    placeholder="Masukan Jumlah Barang" name="jumlah[]" required
                                                    min="0">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="add-list" data-barang='{{$barang}}'>+
                                            Permintaan</button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal + permintaan --}}

                <table class="table table-bordered" style="margin-top: 1%">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Karyawan</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Jumlah Permintaan</th>
                            <th scope="col">Tanggal Permintaan</th>
                            <th scope="col">Status Permintaan</th>
                            <th scope="col">Status Item</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($detail_permintaan as $dt_prmntaan)
                            @if(Auth::guard('karyawan')->user()->id == $dt_prmntaan->user_id)
                                <tr>
                                    <td>{{$no++}}.</td>
                                    <td>{{ $dt_prmntaan->nama_lengkap }}</td>
                                    <td>{{ $dt_prmntaan->nama_barang}}</td>
                                    <td>{{ $dt_prmntaan->jumlah_permintaan}}</td>
                                    <td>{{ date('d F Y', strtotime($dt_prmntaan->tgl_permintaan)) }}</td>
                                    <td>{{ $dt_prmntaan->status_permintaan }}</td>
                                    <td>{{ $dt_prmntaan->status}}</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                            data-target="{{'#edituser'.$no}}">Edit</a>
                                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                            data-target="{{'#hapus'.$no}}">Hapus</button>
                                    </td>
                                </tr>
                            @endif
                            <!-- Modal Edit permintaan-->
                            <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data Permintaan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="/karyawan/transaksi/edit-permintaan/{{$dt_prmntaan->id}}"
                                                enctype="multipart/form-data">
                                                @method('patch')
                                                @csrf
                                                <div class="form-group">
                                                    <label for="nama_barang">Nama Barang</label>
                                                    <select class="col-8 form-control" name="id_barang" disabled required>
                                                        <option value="{{$dt_prmntaan->barang_id}}">
                                                            {{$dt_prmntaan->nama_barang}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="jumlah">Jumlah</label>
                                                    <input type="number" class="form-control col-8 "
                                                        placeholder="Masukan Jumlah Barang"
                                                        value="{{$dt_prmntaan->jumlah_permintaan}}" name="jumlah" required
                                                        min="0">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- modal hapus barang   --}}
                            <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Hapus Permintaan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="/karyawan/transaksi/delete-permintaan/{{$dt_prmntaan->id}}"
                                                enctype="multipart/form-data">
                                                @method('delete')
                                                @csrf

                                                Apakah Yakin akan menghapus permintaan
                                                {{$dt_prmntaan->nama_lengkap}}.?<br><br>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '#add-list', function (event) {
            event.preventDefault();

            let barang = $(this).data('barang');
            let element = `
                <div class="isi">
                    <div class="form-group">
                        <label for="barang">Barang</label>
                        <select class="col-8 form-control" name="id_barang[]" required>
                            <option>Pilih Barang</option>
                            `;
            $.each(barang, function (index, value) {
                element +=
                    `<option value='${value.id}'>${value.nama_barang}</option>`
            });
            element += `
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="jumlah">Jumlah</label>
                        <input type="number" class="form-control col-8 "
                            placeholder="Masukan Jumlah Barang" name="jumlah[]"
                            required min="0">
                    </div>
                </div>
                `;

            $('.permintaan').append(element);
        });
    });
</script>

@endpush
