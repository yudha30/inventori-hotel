@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Dashboard</h3>
        <p class="panel-subtitle">Karyawan</p>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="metric">
                    <span class="icon"><i class="fa fa-line-chart"></i></span>
                    <p>
                        <span class="number">{{ $perbaikan }}</span>
                        <span class="title">Perbaikan</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="metric">
                    <span class="icon"><i class="fa fa-eye"></i></span>
                    <p>
                        <span class="number">{{ $pengecekan }}</span>
                        <span class="title">Pengecekan</span>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="metric">
                    <span class="icon"><i class="fa fa-bar-chart"></i></span>
                    <p>
                        <span class="number">{{ $permintaan }}</span>
                        <span class="title">Permintaan</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
