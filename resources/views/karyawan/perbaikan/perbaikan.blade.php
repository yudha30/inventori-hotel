@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Perbaikan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Perbaikan
                </button>
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <!-- Modal + permintaan -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="panel-title" id="exampleModalLabel">Perbaikan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/karyawan/transaksi/create-detail-perbaikan"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" name="id_user"
                                            value="{{Auth::guard('karyawan')->user()->id}}">
                                    </div>
                                    <div class="permintaan">
                                        <div class="isi">
                                            <div class="form-group">
                                                <label for="barang">Inventori</label>
                                                <select class="col-8 form-control" name="id_inventori[]" required>
                                                    <option value="">Pilih Inventori</option>
                                                    @foreach ($tb_inventori as $inv)
                                                    <option value="{{$inv->id}}">{{$inv->nama_inventori}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="keterangan">Keterangan</label><br>
                                                <textarea class="form-control" placeholder="Masukan Keterangan" value="" name="keterangan[]" required id="" rows="4" cols="80"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="add-list" data-inventori='{{$tb_inventori}}'>+
                                            Perbaikan</button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal + perbaikan --}}

                <table class="table table-bordered" style="margin-top: 1%;">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">No</th>
                            <th scope="col" width="10%">Inventori</th>
                            <th scope="col" width="10%">Barang</th>
                            <th scope="col" width="10%">Ruangan</th>
                            <th scope="col" width="15%">Keterangan</th>
                            <th scope="col" width="10%">tanggal</th>
                            <th scope="col" width="10%">Status</th>
                            <th scope="col" width="10%">Status Item</th>
                            <th scope="col" width="20%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($detail_perbaikan as $dt_prbaikan)
                            @if ($dt_prbaikan->user_id == Auth::guard('karyawan')->user()->id)
                                <tr>
                                    <td>{{$no++}}.</td>
                                    <td>{{ $dt_prbaikan->nama_inventori}}</td>
                                    @if ($dt_prbaikan->barang_id==null)
                                    <td>-</td>
                                    @else
                                    <td>{{ $dt_prbaikan->nama_barang }}</td>
                                    @endif
                                    @if ($dt_prbaikan->ruangan_id==null)
                                    <td>-</td>
                                    @else
                                    <td>{{ $dt_prbaikan->nama_ruangan }}</td>
                                    @endif
                                    <td>{{ $dt_prbaikan->keterangan}}</td>
                                    <td>{{ date('d F Y', strtotime($dt_prbaikan->tgl_perbaikan))}}</td>
                                    <td>{{ $dt_prbaikan->status_perbaikan}}</td>
                                    <td>{{ $dt_prbaikan->status}}</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                            data-target="{{'#edituser'.$no}}">Edit</a>
                                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                            data-target="{{'#hapus'.$no}}">Hapus</button>
                                    </td>
                                </tr>
                            @endif

                        <!-- Modal Edit perbaikan-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="panel-title" id="exampleModalLabel">Edit Data Perbaikan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/karyawan/transaksi/edit-perbaikan/{{$dt_prbaikan->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_inventori">Nama Inventori</label>
                                                <select class="col-8 form-control" name="id_inventori" disabled>
                                                    <option value="{{$dt_prbaikan->inventori_id}}" selected>{{$dt_prbaikan->nama_inventori}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="keterangan">Keterangan</label>
                                                <textarea class="form-control" placeholder="Masukan Keterangan" value="{{$dt_prbaikan->keterangan}}" name="keterangan" cols="80" rows="4" required>{{$dt_prbaikan->keterangan}}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus perbaikan   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Perbaikan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/karyawan/transaksi/delete-perbaikan/{{$dt_prbaikan->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus perbaikan
                                            {{$dt_prbaikan->nama_lengkap}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
<script>
    $(document).ready(function () {
        $(document).on('click', '#add-list', function (event) {
            event.preventDefault();

            let inventori = $(this).data('inventori');

            let element = `
            <div class="isi">
                <div class="form-group">
                    <label for="inventori">Inventori</label>
                    <select class="col-8 form-control" name="id_inventori[]" required>
                        <option>Pilih Inventori</option>
                        `;
            $.each(inventori, function (index, value) {
                element += `<option value='${value.id}'>${value.nama_inventori}</option>`
            });
            element += `
                    </select>
                </div>

                <div class="form-group">
                    <label for="keterangan">Keterangan</label><br>
                    <textarea class="form-control" placeholder="Masukan Keterangan" value="" name="keterangan[]" required id="" rows="4" cols="80"></textarea>
                </div>
            </div>
            `;

            $('.permintaan').append(element);
        });
    })
</script>
@endpush
