@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Pengecekan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Pengecekan
                </button>
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <!-- Modal + pengecekan -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Pengecekan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/karyawan/transaksi/create-detail-pengecekan"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" name="id_user"
                                            value="{{Auth::guard('karyawan')->user()->id}}">
                                    </div>
                                    <div class="permintaan">
                                        <div class="isi">
                                            <div class="form-group">
                                                <label for="barang">Inventori</label>
                                                <select class="col-8 form-control" name="id_inventori[]" required>
                                                    <option value="">Pilih Inventori</option>
                                                    @foreach ($tb_inventori as $inv)
                                                    <option value="{{$inv->id}}">{{$inv->nama_inventori}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="keadaan_barang">Keadaan Barang</label>
                                                <select class="form-control col-8" name="keadaan_barang[]" required>
                                                    <option value="">Pilih Keadaan</option>
                                                    <option value="Baik">Baik</option>
                                                    <option value="Rusak">Rusak</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="keterangan">Keterangan</label><br>
                                                <textarea placeholder="Masukan Keterangan" value="" name="keterangan[]" class="form-control" required id="" rows="4" cols="80"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="add-list" data-inventori='{{$tb_inventori}}'>+
                                            Pengecekan</button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal + pengecekan --}}

                <table class="table table-bordered" style="margin-top: 1%">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Karyawan</th>
                            <th scope="col">Nama Inventori</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Ruangan</th>
                            <th scope="col">Keadaan Barang</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($detail_pengecekan as $dt_pengecekan)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $dt_pengecekan->nama_lengkap}}</td>
                            <td>{{ $dt_pengecekan->nama_inventori}}</td>
                            @if ($dt_pengecekan->barang_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $dt_pengecekan->nama_barang }}</td>
                            @endif
                            @if ($dt_pengecekan->ruangan_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $dt_pengecekan->nama_ruangan }}</td>
                            @endif
                            <td>{{ $dt_pengecekan->keadaan_barang}}</td>
                            <td>{{ $dt_pengecekan->keterangan}}</td>
                            <td>
                                <a href="" class="btn btn-warning" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit pengecekan-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Pengecekan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/karyawan/transaksi/edit-pengecekan/{{$dt_pengecekan->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_inventori">Nama Inventori</label>
                                                <select class="col-8 form-control" name="id_inventori" required>
                                                    <option value="{{$dt_pengecekan->inventori_id}}">
                                                        {{$dt_pengecekan->nama_inventori}}</option>
                                                    @foreach ($tb_inventori as $inv)
                                                    <option value="{{$inv->id}}">{{$inv->nama_inventori}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="keterangan">Keterangan</label>
                                                    <textarea placeholder="Masukan Keterangan" value="{{$dt_pengecekan->keterangan}}" name="keterangan" class="form-control" cols="80" rows="4" required>{{$dt_pengecekan->keterangan}}</textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus pengecekan   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Perbaikan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/karyawan/transaksi/delete-pengecekan/{{$dt_pengecekan->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus perbaikan
                                            {{$dt_pengecekan->nama_lengkap}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
<script>
    $(document).ready(function () {
        $(document).on('click', '#add-list', function (event) {
            event.preventDefault();
            let inventori = $(this).data('inventori');

            let element = `
            <div class="isi">
                <div class="form-group">
                    <label for="inventori">Inventori</label>
                    <select class="col-8 form-control" name="id_inventori[]" required>
                        <option>Pilih Inventori</option>
                        `;
            $.each(inventori, function (index, value) {
                element += `<option value='${value.id}'>${value.nama_inventori}</option>`
            });
            element += `
                    </select>
                </div>

                <div class="form-group">
                    <label for="keadaan_barang">Keadaan Barang</label>
                    <select class="form-control col-8" name="keadaan_barang[]" required>
                        <option value="">Pilih Keadaan</option>
                        <option value="Baik">Baik</option>
                        <option value="Rusak">Rusak</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="keterangan">Keterangan</label><br>
                    <textarea class="form-control" placeholder="Masukan Keterangan" value="" name="keterangan[]" required id="" rows="4" cols="80"></textarea>
                </div>
            </div>
            `;

            $('.permintaan').append(element);

        });
    })

</script>
@endpush
