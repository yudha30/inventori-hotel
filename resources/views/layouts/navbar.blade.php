<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        {{-- <a href="javascript:void(0)"> --}}
            {{-- <img src="{{ asset('image/logo.jpg') }}" height="21" class="img-responsive logo"> --}}
        {{-- </a> --}}
        <span style="font-size: 20px; font-weight: bold">Hotel jogja Inn</span>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img
                            src="{{ asset('template/assets/img/user.png') }}" class="img-circle" alt="Avatar">
                            @if (Auth::guard('admin-sistem')->user())
                                <span>{{Auth::guard('admin-sistem')->user()->nama_lengkap}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                            @elseif (Auth::guard('admin-inventori')->user())
                                <span>{{Auth::guard('admin-inventori')->user()->nama_lengkap}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                            @elseif (Auth::guard('karyawan')->user())
                                <span>{{Auth::guard('karyawan')->user()->nama_lengkap}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                            @else
                                <span>{{Auth::guard('manager')->user()->nama_lengkap}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                            @endif
                    <ul class="dropdown-menu">
                        {{-- <li><a href="#"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
                        <li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
                        <li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li> --}}
                        <li><a href="{{ url('logout') }}"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
