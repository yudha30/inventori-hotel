<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                {{-- Sidebar Admin Sistem --}}
                @if (Auth::guard('admin-sistem')->user())
                <li>
                    <a href="{{ url('/') }}" @if(Request::segment(2)=='dashboard' ) class="active" @endif>
                        <i class="lnr lnr-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('admin-sistem/user') }}" @if(Request::segment(2)=='user' ) class="active" @endif class="">
                        <i class="lnr lnr-user"></i>
                        <span>User</span>
                    </a>
                </li>
                @endif

                {{-- Sidebar Karyawan--}}
                @if (Auth::guard('karyawan')->user())
                <li>
                    <a href="{{ url('/') }}" @if(Request::segment(2)=='dashboard' ) class="active" @endif>
                        <i class="lnr lnr-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i>
                        <span>Transaksi</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages" class="collapse ">
                        <ul class="nav">
                            <li><a href="{{ url('karyawan/transaksi/permintaan') }}" class=""><i class=""></i> <span>Permintaan</span></a></li>
                            <li><a href="{{ url('karyawan/transaksi/perbaikan') }}" class=""><i class=""></i> <span>Perbaikan</span></a></li>
                            <li><a href="{{ url('karyawan/transaksi/pengecekan') }}" class=""><i class=""></i> <span>Pengecekan</span></a></li>
                            <li><a href="{{ url('karyawan/transaksi/penghapusan') }}" class=""><i class=""></i> <span>Penghapusan</span></a></li>
                        </ul>
                    </div>
                </li>

                @endif

                {{-- Sidebar Manager --}}
                @if(Auth::guard('manager')->user())
                <li>
                    <a href="{{ url('/') }}" @if(Request::segment(2)=='dashboard' ) class="active" @endif>
                        <i class="lnr lnr-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#subPages" data-toggle="collapse" class="@if(Request::segment(2) == 'transaksi') active @else collapsed @endif">
                        <i class="lnr lnr-file-empty"></i>
                        <span>Transaksi</span>
                        <i class="icon-submenu lnr lnr-chevron-left"></i>
                    </a>
                    <div id="subPages" class="@if(Request::segment(2) == 'transaksi') collapse in @else collapse @endif">
                        <ul class="nav">
                            <li><a href="{{ url('manager/transaksi/transaksi-permintaan') }}" class="">Permintaan</a></li>
                            <li><a href="{{ url('manager/transaksi/transaksi-perbaikan') }}" class="">Perbaikan</a></li>
                            <li><a href="{{ url('manager/transaksi/transaksi-penghapusan') }}" class="">Penghapusan</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#laporan-manager" data-toggle="collapse" class="@if(Request::segment(2) == 'laporan') active @else collapsed @endif">
                        <i class="lnr lnr-file-empty"></i>
                        <span>Laporan</span>
                        <i class="icon-submenu lnr lnr-chevron-left"></i>
                    </a>
                    <div id="laporan-manager" class="@if(Request::segment(2) == 'laporan') collapse in @else collapse @endif">
                        <ul class="nav">
                            <li><a href="{{ url('manager/laporan/permintaan') }}" class="">Permintaan</a></li>
                            <li><a href="{{ url('manager/laporan/pengecekan') }}" class="">Pengecekan</a></li>
                            <li><a href="{{ url('manager/laporan/perbaikan') }}" class="">Perbaikan</a></li>
                            <li><a href="{{ url('manager/laporan/penghapusan') }}" class="">Penghapusan</a></li>
                            <li><a href="{{ url('manager/laporan/barang') }}" class="">Barang</a></li>
                            <li><a href="{{ url('manager/laporan/ruangan') }}" class="">Ruangan</a></li>
                            <li><a href="{{ url('manager/laporan/inventori') }}" class="">Inventori</a></li>
                        </ul>
                    </div>
                </li>
                @endif

                {{-- Sidebar Admin Inventory --}}
                @if(Auth::guard('admin-inventori')->user())
                <li>
                    <a href="{{ url('/') }}" @if(Request::segment(2)=='dashboard' ) class="active" @endif><i class="lnr lnr-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i>
                        <span>Master Data</span>
                        <i class="icon-submenu lnr lnr-chevron-left"></i>
                    </a>
                    <div id="subPages" class="collapse ">
                        <ul class="nav">
                            <li><a href="{{ url('admin-inventori/barang') }}" class="">Barang</a></li>
                            <li><a href="{{ url('admin-inventori/kategori') }}" class="">Kategori</a></li>
                            <li><a href="{{ url('admin-inventori/supplier') }}" class="">Supplier</a></li>
                            <li><a href="{{ url('admin-inventori/inventori') }}" class="">Inventori</a></li>
                            <li><a href="{{ url('admin-inventori/ruangan') }}" class="">Ruangan</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#transaksi" data-toggle="collapse" class="collapsed"><i class="lnr lnr-file-empty"></i>
                        <span>Transaksi</span>
                        <i class="icon-submenu lnr lnr-chevron-left"></i>
                    </a>
                    <div id="transaksi" class="collapse ">
                        <ul class="nav">
                            <li><a href="{{ url('admin-inventori/permintaan') }}" class="">Permintaan</a></li>
                            <li><a href="{{ url('admin-inventori/perbaikan') }}" class="">Perbaikan</a></li>
                            <li><a href="{{ url('admin-inventori/transaksi-penghapusan') }}" class="">Penghapusan</a></li>
                        </ul>
                    </div>
                </li>
                @endif
            </ul>
        </nav>
    </div>
</div>
