@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Dashboard</h3>
        <p class="panel-subtitle">Admin Sistem</p>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="metric">
                    <span class="icon"><i class="fa fa-line-chart"></i></span>
                    <p>
                        <span class="number">{{ $admin_sistem }}</span>
                        <span class="title">Admin Sistem</span>
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="metric">
                    <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                    <p>
                        <span class="number">{{ $admin_inventori }}</span>
                        <span class="title">Admin Inventori</span>
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="metric">
                    <span class="icon"><i class="fa fa-eye"></i></span>
                    <p>
                        <span class="number">{{ $karyawan }}</span>
                        <span class="title">Karyawan</span>
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="metric">
                    <span class="icon"><i class="fa fa-bar-chart"></i></span>
                    <p>
                        <span class="number">{{ $manager }}</span>
                        <span class="title">Manager</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
