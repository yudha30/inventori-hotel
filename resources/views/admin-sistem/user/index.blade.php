@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Data User</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + User
                </button>
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <!-- Modal + user -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">User</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/admin-sistem/create-user" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama_lengkap">Nama Lengkap</label>
                                        <input type="text" class="form-control col-8 "
                                            placeholder="Masukan Nama Lengkap" name="nama_lengkap"
                                            value="{{old('nama_lengkap')}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="level">Level</label>
                                        <select class="form-control col-8" name="level" required>
                                            <option>Pilih Level</option>
                                            <option value="1">Admin</option>
                                            <option value="2">Admin inventory</option>
                                            <option value="3">Manager</option>
                                            <option value="4">Karyawan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="no hp">No Telp</label>
                                        <input type="number" class="form-control col-8 " placeholder="Masukan No Telp"
                                            name="no_telp" value="{{old('no_telp')}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" min=0 class="form-control col-8 "
                                            placeholder="Masukan Username" name="username" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control col-8 "
                                            placeholder="Masukan Password" name="password" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal + user --}}
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Lengkap</th>
                            <th scope="col">Username</th>
                            <th scope="col">No Telp</th>
                            <th scope="col">Level</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($user as $usr)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $usr->nama_lengkap }}</td>
                            <td>{{ $usr->username }}</td>
                            <td>{{ $usr->no_hp }}</td>
                            @if($usr->level==1)
                            <td>Admin</td>
                            @elseif($usr->level==2)
                            <td>Admin Inventory</td>
                            @elseif($usr->level==3)
                            <td>Manager</td>
                            @else
                            <td>Karyawan</td>
                            @endif
                            <td>
                                <a href="" class="btn btn-warning" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit User-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data User</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-sistem/edit-user/{{$usr->id}}"
                                            enctype="multipart/form-data" onkeyup="validation()">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_lengkap">Nama Lengkap</label>
                                                <input type="hidden" class="form-control col-8" name="id"
                                                    value="{{$usr->id}}" required>
                                                <input type="text" class="form-control col-8"
                                                    placeholder="Masukan Nama Lengkap" name="nama_lengkap"
                                                    value="{{$usr->nama_lengkap}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="level">Level</label>
                                                <select class="form-control col-8" name="level" required>
                                                    @if ($usr->level==1)
                                                    <option value="1">Admin</option>
                                                    <option value="2">Admin inventory</option>
                                                    <option value="3">Manager</option>
                                                    <option value="4">Karyawan</option>
                                                    @elseif ($usr->level==2)
                                                    <option value="2">Admin Inventory</option>
                                                    <option value="1">Admin</option>
                                                    <option value="3">Manager</option>
                                                    <option value="4">Karyawan</option>
                                                    @elseif ($usr->level==3)
                                                    <option value="3">Manager</option>
                                                    <option value="1">Admin</option>
                                                    <option value="2">Admin inventory</option>
                                                    <option value="4">Karyawan</option>
                                                    @else
                                                    <option value="4">Karyawan</option>
                                                    <option value="1">Admin</option>
                                                    <option value="2">Admin inventory</option>
                                                    <option value="3">Manager</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="no hp">No Telp</label>
                                                <input type="number" class="form-control col-8"
                                                    placeholder="Masukan No Telp" name="no_telp" value="{{$usr->no_hp}}"
                                                    required>
                                            </div>
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" class="form-control col-8"
                                                    placeholder="Masukan Username" name="username"
                                                    value="{{$usr->username}}" required>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus data   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus User</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-sistem/delete-user/{{$usr->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus {{$usr->nama_lengkap}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
