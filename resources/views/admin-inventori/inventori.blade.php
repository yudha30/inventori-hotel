@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Inventori</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Inventori
                </button>
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif
                @if (session('gagal'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('gagal') }}
                </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger col-4 mt-2 mb-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <!-- Modal + inventori -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Inventori</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/admin-inventori/create-inventori" enctype="multipart/form-data"
                                    id="form-submit">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama_inventori">Nama Inventori</label>
                                        <input type="text" class="form-control col-8 " placeholder="Masukan Nama Inventori"
                                            name="nama_inventori" id="add_nama_inventori"
                                            required readonly>
                                    </div>

                                    <div class="form-group">
                                        <label for="jenis_inventori">Jenis Inventori</label>
                                        <select class="form-control" name="jenis-inventori" id="add_jenis_inventori" required>
                                            <option value="">Pilih Jenis Inventori</option>
                                            <option value="barang">Barang</option>
                                            <option value="ruangan">Ruangan</option>
                                        </select>
                                    </div>
                                    <div class="form-group barang" hidden>
                                        <label for="nama_barang">Nama Barang</label>
                                        <select class="form-control col-8" id="add_barang" name="id_barang">
                                            <option value="">Pilih Barang</option>
                                            @foreach ($barang as $brg)
                                            <option value="{{$brg->id}}">{{$brg->nama_barang}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group ruangan" hidden>
                                        <label for="nama_ruangan">Nama Ruangan</label>
                                        <select class="form-control col-8" id="add_ruangan" name="id_ruangan">
                                            <option value="">Pilih Ruangan</option>
                                            @foreach ($ruangan as $ruang)
                                            <option value="{{$ruang->id}}">{{$ruang->nama_ruangan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="keadaan_barang">Keadaan</label>
                                        <select class="form-control col-8" name="keadaan_barang" id="add_keadaan" required>
                                            <option value="">Pilih Keadaan</option>
                                            <option value="Baik">Baik</option>
                                            <option value="Rusak">Rusak</option>
                                            <option value="Diperbaiki">Diperbaiki</option>
                                            <option value="Dihapus">Dihapus</option>
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal + inventori --}}
                <table class="table table-bordered" style="margin-top: 1%">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">No</th>
                            <th scope="col" width="15%">Nama Inventori</th>
                            <th scope="col" width="20%">Nama Barang</th>
                            <th scope="col" width="20%">Ruangan</th>
                            <th scope="col" width="20%">Keadaan Barang</th>
                            <th scope="col" width="20%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($inventori as $inv)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $inv->nama_inventori }}</td>
                            @if ($inv->barang_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $inv->nama_barang }}</td>
                            @endif
                            @if ($inv->ruangan_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $inv->nama_ruangan }}</td>
                            @endif
                            <td>{{ $inv->keadaan_barang }}</td>
                            <td>
                                <a href="" class="btn btn-warning btn-edit btn-sm" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}" data-number="{{$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit inventori-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data inventori</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/edit-inventori/{{$inv->id}}"
                                            enctype="multipart/form-data" onkeyup="validation()">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_inventori">Nama inventori</label>
                                                <input type="hidden" class="form-control col-8" name="id"
                                                    value="{{$inv->id}}" required>
                                                <input type="text" class="form-control col-8"
                                                    placeholder="Masukan Nama inventori" name="nama_inventori"
                                                    value="{{$inv->nama_inventori}}" required readonly>
                                            </div>
                                            @if ($inv->barang_id!=null)
                                            <div class="form-group">
                                                <label for="nama_barang">Nama Barang</label>
                                                <select class="form-control col-8" id="update_barang_{{$no}}"
                                                    name="id_barang" class="update_barang" data-id="{{$no}}" disabled required>
                                                    <option value=""> Pilih Barang </option>
                                                    @foreach ($barang as $brg)
                                                    <option @if($inv->barang_id == $brg->id) selected @endif value="{{$brg->id}}">{{$brg->nama_barang}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @elseif($inv->ruangan_id!=null)
                                            <div class="form-group">
                                                <label for="nama_ruangan">Nama Ruangan</label>
                                                <select class="form-control col-8" id="update_ruangan_{{$no}}"
                                                    name="id_ruangan" class="update_ruangan" data-id="{{$no}}" disabled required>
                                                    <option value=""> Pilih Ruangan </option>
                                                    @foreach ($ruangan as $ruang)
                                                    <option value="{{$ruang->id}}" @if($inv->ruangan_id == $ruang->id) selected @endif>{{$ruang->nama_ruangan}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="keadaan_barang">Keadaan Barang</label>
                                                <select class="form-control col-8" id="update_keadaan_{{$no}}"
                                                    name="keadaan_barang" required>
                                                    <option value="">Pilih Keadaan Barang</option>
                                                    <option value="Baik" @if($inv->keadaan_barang == "Baik") selected @endif>Baik</option>
                                                    <option value="Rusak" @if($inv->keadaan_barang == "Rusak") selected @endif >Rusak</option>
                                                    <option value="Diperbaiki" @if($inv->keadaan_barang == "Diperbaiki") selected @endif >Diperbaiki</option>
                                                    <option value="Dihapus" @if($inv->keadaan_barang == "Dihapus") selected @endif >Dihapus</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus inventori   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus inventori</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/delete-inventori/{{$inv->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus {{$inv->nama_inventori}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </tbody>
                </table>
                {{$inventori->links()}}
            </div>
        </div>
    </div>
</div>

@endsection
@push('script')
<script>
    $(document).ready(function () {
        $('select[name="jenis-inventori"]').on('change', function () {

            var jenis_inventori = $('select[name="jenis-inventori"]').val();
            if (jenis_inventori == "") {
                $('.barang').hide();
                $('.ruangan').hide();
                document.getElementById('add_ruangan').removeAttribute('required')
                document.getElementById('add_barang').removeAttribute('required')
            } else if (jenis_inventori == "barang") {
                $('.barang').show();
                $('.ruangan').hide();
                document.getElementById('add_barang').setAttribute('required', 'required')
                document.getElementById('add_ruangan').removeAttribute('required')
            } else {
                $('.barang').hide();
                $('.ruangan').show();
                document.getElementById('add_ruangan').setAttribute('required', 'required')
                document.getElementById('add_barang').removeAttribute('required')
            }
        })

        $(document).on('change','#add_barang',function(event){
            if($(this).val() != ''){
                var date = new Date();
                var year = date.getFullYear();
                var month = date.getMonth();
                let month_romawi = '';

                if(month == 0){
                    month_romawi = 'I'
                }
                else if(month == 1){
                    month_romawi = 'II'
                }
                else if(month == 2){
                    month_romawi = 'III'
                }
                else if(month == 3){
                    month_romawi = 'IV'
                }
                else if(month == 4){
                    month_romawi = 'V'
                }
                else if(month == 5){
                    month_romawi = 'VI'
                }
                else if(month == 6){
                    month_romawi = 'VII'
                }
                else if(month == 7){
                    month_romawi = 'VIII'
                }
                else if(month == 8){
                    month_romawi = 'IX'
                }
                else if(month == 9){
                    month_romawi = 'X'
                }
                else if(month == 10){
                    month_romawi = 'XI'
                }
                else{
                    month_romawi = 'XII'
                }

                let string = $('#add_barang option:selected').text();
                string = string.replace(/\s/g, '').toUpperCase();
                let kode = '';

                let length = string.length;

                if(length > 3){
                    for(let i=0; i < length; i++){
                        if(i%2==0){
                            kode += string.charAt(i)
                        }
                    }
                }
                else{
                    kode = string;
                }

                let nama_inventori = `${year}/${month_romawi}/${kode}/B.${Math.floor(Math.random() * 10000) + 1}`;

                $('#add_nama_inventori').val(nama_inventori);
            }else{
                $('#add_nama_inventori').val('');
            }
        });

        $(document).on('change','#add_ruangan',function(event){
            if($(this).val() != ''){
                var date = new Date();
                var year = date.getFullYear();
                var month = date.getMonth();
                let month_romawi = '';

                if(month == 0){
                    month_romawi = 'I'
                }
                else if(month == 1){
                    month_romawi = 'II'
                }
                else if(month == 2){
                    month_romawi = 'III'
                }
                else if(month == 3){
                    month_romawi = 'IV'
                }
                else if(month == 4){
                    month_romawi = 'V'
                }
                else if(month == 5){
                    month_romawi = 'VI'
                }
                else if(month == 6){
                    month_romawi = 'VII'
                }
                else if(month == 7){
                    month_romawi = 'VIII'
                }
                else if(month == 8){
                    month_romawi = 'IX'
                }
                else if(month == 9){
                    month_romawi = 'X'
                }
                else if(month == 10){
                    month_romawi = 'XI'
                }
                else{
                    month_romawi = 'XII'
                }

                let string = $('#add_ruangan option:selected').text();
                string = string.replace(/\s/g, '').replace(/\./g,' ').toUpperCase();
                let kode = '';

                let length = string.length;
                if(length > 3){
                    for(let i=0; i < length; i++){
                        if(i%2==0){
                            kode += string.charAt(i)
                        }
                    }
                }
                else{
                    kode = string;
                }

                let nama_inventori = `${year}/${month_romawi}/${kode}/R.${Math.floor(Math.random() * 10000) + 1}`;

                $('#add_nama_inventori').val(nama_inventori);
            }
            else{
                $('#add_nama_inventori').val('');
            }
        });

        $(document).on('change','#add_jenis_inventori',function(event){
            $('#add_nama_inventori').val('');
        })
    });

</script>
@endpush
