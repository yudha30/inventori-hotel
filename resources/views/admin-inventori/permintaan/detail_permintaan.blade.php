@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Permintaan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Permintaan
                </button> --}}
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Karyawan</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Nama Supplier</th>
                            <th scope="col">Jumlah Permintaan</th>
                            <th scope="col">Tanggal Permintaan</th>
                            <th scope="col">Status</th>
                            <th scope="col">Status Item</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($detail_permintaan as $dt_prmntaan)

                        @if ($dt_prmntaan->status_permintaan != "Permintaan")

                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $dt_prmntaan->nama_lengkap }}</td>
                            <td>{{ $dt_prmntaan->nama_barang}}</td>
                            @if($dt_prmntaan->supplier_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $dt_prmntaan->nama_supplier}}</td>
                            @endif
                            <td>{{ $dt_prmntaan->jumlah_permintaan}}</td>
                            <td>{{ date('d F Y', strtotime($dt_prmntaan->tgl_permintaan)) }}</td>
                            <td>{{ $dt_prmntaan->status_permintaan }}</td>
                            <td>{{ $dt_prmntaan->status}}</td>
                            <td>
                                <a href="" class="btn btn-warning" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit permintaan-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Permintaan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/admin-inventori/detail/edit-permintaan/{{$dt_prmntaan->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_supplier">Nama Supplier</label>
                                                <select class="col-8 form-control" name="id_supplier" required>
                                                    <option value="" selected>Pilih Supplier</option>
                                                    @foreach ($supplier as $supp)
                                                        <option value="{{$supp->id}}" @if($dt_prmntaan->supplier_id == $supp->id) selected @endif>{{$supp->nama_supplier}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="jumlah">Jumlah Permintaan</label>
                                                <input type="text" id="jumlah" name="jumlah_permintaan" class="form-control" value="{{ $dt_prmntaan->jumlah_permintaan }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="keterangan">Status Item</label>
                                                <select class="col-8 form-control" name="status" required>
                                                    <option value="" selected>Pilih Status Item</option>
                                                    <option value="Dalam Proses" @if($dt_prmntaan->status == 'Dalam Proses') selected @endif>Dalam Proses</option>
                                                    <option value="Terpenuhi" @if($dt_prmntaan->status == 'Terpenuhi') selected @endif>Terpenuhi</option>
                                                    <option value="Tidak Terpenuhi" @if($dt_prmntaan->status == 'Tidak Terpenuhi') selected @endif>Tidak Terpenuhi</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus barang   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Permintaan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/admin-inventori/detail/delete-permintaan/{{$dt_prmntaan->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus permintaan
                                            {{$dt_prmntaan->nama_lengkap}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
@endpush
