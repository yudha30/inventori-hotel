@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Permintaan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Permintaan
                </button> --}}
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">No</th>
                            <th scope="col" width="30%">Nama Karyawan</th>
                            <th scope="col" width="20%">Tanggal Permintaan</th>
                            <th scope="col" width="20%">Status</th>
                            <th scope="col" width="25%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($permintaan as $data)
                            @if ($data->status != "Permintaan" && $data->status != "Tidak Acc Manager")
                                <tr>
                                    <td>{{$loop->iteration}}.</td>
                                    <td>{{$data->user->nama_lengkap}}</td>
                                    <td>{{ date('d F Y', strtotime($data->tgl_permintaan))}}</td>
                                    <td>{{ $data->status}}</td>
                                    <td>
                                        <a href="{{ url('admin-inventori/detail/permintaan') }}/{{$data->id}}" class="btn btn-primary btn-sm">Detail</a>
                                        <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                            data-target="{{'#edituser'.$loop->iteration}}">Edit</a>
                                        <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                            data-target="{{'#hapus'.$loop->iteration}}">Hapus</button>
                                    </td>
                                </tr>

                                <!-- Modal Edit permintaan-->
                                <div class="modal fade" id="{{'edituser'.$loop->iteration}}" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Data Permintaan</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post"
                                                    action="/admin-inventori/permintaan-edit/{{$data->id}}"
                                                    enctype="multipart/form-data">
                                                    @method('patch')
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="keterangan">Status</label>
                                                        <select class="col-8 form-control" name="status" required>
                                                            <option value="">Pilih Status</option>
                                                            <option value="Acc Admin">Acc Admin</option>
                                                            <option value="Tidak Acc Admin">Tidak Acc Admin</option>
                                                        </select>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-primary">Edit</button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- modal hapus barang   --}}
                                <div class="modal fade" id="{{'hapus'.$loop->iteration}}" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Hapus Permintaan</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post"
                                                    action="/admin-inventori/permintaan-delete/{{$data->id}}"
                                                    enctype="multipart/form-data">
                                                    @method('delete')
                                                    @csrf

                                                    Apakah Yakin akan menghapus permintaan
                                                    {{$data->user->nama_lengkap}}.?<br><br>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Batal</button>
                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
@endpush
