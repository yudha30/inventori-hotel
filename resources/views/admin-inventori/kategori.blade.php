@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Kategori</h3>
    </div>
    <div class="row">
        <div class="col">
            <div class="panel-body">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Kategori
                </button>
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <!-- Modal + kategori -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Kategori</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/admin-inventori/create-kategori"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama_kategori">Nama Kategori</label>
                                        <input type="text" class="form-control col-8 "
                                            placeholder="Masukan Nama Kategori" name="nama_kategori"
                                            value="{{old('nama_lengkap')}}" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal + kategori --}}
                <table class="table table-bordered" style="margin-top: 1%">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">No</th>
                            <th scope="col" width="70%">Nama Kategori</th>
                            <th scope="col" width="15%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($kategori as $ktgr)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $ktgr->nama_kategori }}</td>
                            <td>
                                <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit Kategori-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Kategori</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/edit-kategori/{{$ktgr->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_kategori">Nama Kategori</label>
                                                <input type="hidden" class="form-control col-8" name="id"
                                                    value="{{$ktgr->id}}" required>
                                                <input type="text" class="form-control col-8"
                                                    placeholder="Masukan Nama Lengkap" name="nama_kategori"
                                                    value="{{$ktgr->nama_kategori}}">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus kategori   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Kategori</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/delete-kategori/{{$ktgr->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus {{$ktgr->nama_kategori}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </tbody>
                </table>
                {{$kategori->links()}}
            </div>
        </div>
    </div>
</div>


@endsection
