@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Supplier</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Supplier
                </button>
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <!-- Modal + supplier -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Supplier</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/admin-inventori/create-supplier"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama_kategori">Nama Supplier</label>
                                        <input type="text" class="form-control col-8 "
                                            placeholder="Masukan Nama Supplier" name="nama_supplier"
                                            value="{{old('nama_lengkap')}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_kategori">Alamat Supplier</label>
                                        <input type="text" class="form-control col-8 "
                                            placeholder="Masukan Alamat Supplier" name="alamat_supplier"
                                            value="{{old('nama_lengkap')}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_kategori">No Telp Supplier</label>
                                        <input type="number" class="form-control col-8 " placeholder="Masukan No Telp"
                                            name="no_telp" value="{{old('nama_lengkap')}}" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal + supplier --}}
                <table class="table table-bordered" style="margin-top: 1%">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">No</th>
                            <th scope="col" width="20%">Nama Supplier</th>
                            <th scope="col" width="40%">Alamat Supplier</th>
                            <th scope="col" width="15%">No Telp Supplier</th>
                            <th scope="col" width="20%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($supplier as $spl)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $spl->nama_supplier }}</td>
                            <td>{{ $spl->alamat_supplier }}</td>
                            <td>{{ $spl->no_hp }}</td>
                            <td>
                                <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit supplier-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Supplier</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/edit-supplier/{{$spl->id}}"
                                            enctype="multipart/form-data" onkeyup="validation()">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_supplier">Nama Supplier</label>
                                                <input type="hidden" class="form-control col-8" name="id"
                                                    value="{{$spl->id}}" required>
                                                <input type="text" class="form-control col-8"
                                                    placeholder="Masukan Nama Supplier" name="nama_supplier"
                                                    value="{{$spl->nama_supplier}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat_supplier">Alamat Supplier</label>
                                                <input type="text" class="form-control col-8"
                                                    placeholder="Masukan Alamat Supplier" name="alamat_supplier"
                                                    value="{{$spl->alamat_supplier}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="no_hp">No Telp Supplier</label>
                                                <input type="number" class="form-control col-8"
                                                    placeholder="Masukan No Telp" name="no_telp" value="{{$spl->no_hp}}"
                                                    required>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus supplier   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Supplier</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/delete-supplier/{{$spl->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus {{$spl->nama_supplier}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </tbody>
                </table>
                {{$supplier->links()}}
            </div>
        </div>
    </div>
</div>


@endsection
