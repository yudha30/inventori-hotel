@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Perbaikan</h3>
    </div>
    <div class="row">
        <div class="col">
            <div class="panel-body">
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Karyawan</th>
                            <th scope="col">Nama Inventori</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Ruangan</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Status</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($detail_perbaikan as $dt_prbaikan)

                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $dt_prbaikan->nama_lengkap}}</td>
                            <td>{{ $dt_prbaikan->nama_inventori}}</td>
                            @if ($dt_prbaikan->barang_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $dt_prbaikan->nama_barang }}</td>
                            @endif
                            @if ($dt_prbaikan->ruangan_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $dt_prbaikan->nama_ruangan }}</td>
                            @endif
                            <td>{{ $dt_prbaikan->keterangan}}</td>
                            <td>{{ $dt_prbaikan->status}}</td>
                            <td>
                                <a href="" class="btn btn-warning" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit perbaikan-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Perbaikan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/detail/edit-perbaikan/{{$dt_prbaikan->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_inventori">Status</label>
                                                <select class="form-control col-8 select_inventori" name="status" required>
                                                    <option value="">Pilih Status</option>
                                                    <option value="Sedang Diperbaiki">Sedang Diperbaiki</option>
                                                    <option value="Selesai Diperbaiki">Selesai Diperbaiki</option>
                                                    <option value="Tidak Bisa Diperbaiki">Tidak Bisa Diperbaiki</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus perbaikan   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Perbaikan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/detail/delete-perbaikan/{{$dt_prbaikan->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus perbaikan
                                            {{$dt_prbaikan->nama_lengkap}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
@endpush
