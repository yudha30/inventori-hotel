@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Perbaikan</h3>
    </div>
    <div class="row">
        <div class="col">
            <div class="panel-body">
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">No</th>
                            <th scope="col" width="50%">Nama Karyawan</th>
                            <th scope="col" width="10%%">Tanggal</th>
                            <th scope="col" width="10%">Status</th>
                            <th scope="col" width="25%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($perbaikan as $data)
                            @if ($data->status != "Perbaikan" && $data->status != "Tidak Acc Manager")
                            <tr>
                                <td>{{$no++}}.</td>
                                <td>{{ $data->user->nama_lengkap}}</td>
                                <td>{{ date('d F Y',strtotime($data->tgl_perbaikan))}}</td>
                                <td>{{ $data->status}}</td>
                                <td>
                                    <a href="{{ url('admin-inventori/detail/perbaikan') }}/{{$data->id}}" class="btn btn-primary btn-sm">Detail</a>
                                    <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                        data-target="{{'#edituser'.$no}}">Edit</a>
                                    <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                        data-target="{{'#hapus'.$no}}">Hapus</button>
                                </td>
                            </tr>

                            <!-- Modal Edit perbaikan-->
                            <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Data Perbaikan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="/admin-inventori/edit-perbaikan/{{$data->id}}"
                                                enctype="multipart/form-data">
                                                @method('patch')
                                                @csrf
                                                <div class="form-group">
                                                    <label for="nama_inventori">Status</label>
                                                    <select class="form-control col-8 select_inventori" name="status" required>
                                                        <option value="" selected>Pilih Status</option>
                                                        <option value="Acc Admin" @if($data->status == 'Acc Admin') selected @endif>Acc Admin</option>
                                                        <option value="Tidak Acc Admin" @if($data->status == 'Tidak Acc Admin') selected @endif>Tidak Acc Admin</option>
                                                    </select>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-primary">Edit</button>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- modal hapus perbaikan   --}}
                            <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Hapus Perbaikan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="/admin-inventori/delete-perbaikan/{{$data->id}}"
                                                enctype="multipart/form-data">
                                                @method('delete')
                                                @csrf

                                                Apakah Yakin akan menghapus perbaikan
                                                {{$data->user->nama_lengkap}}.?<br><br>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Batal</button>
                                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
@endpush
