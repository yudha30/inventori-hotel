@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Data Barang</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Barang
                </button>
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger col-4 mt-2 mb-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <!-- Modal + barang -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Barang</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/admin-inventori/create-barang"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama_barang">Nama Barang</label>
                                        <input type="text" class="form-control col-8 " placeholder="Masukan Nama Barang"
                                            name="nama_barang" value="{{old('nama_barang')}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="kategori">Kategori</label>
                                        <select class="col-8 form-control" name="id_kategori" required="required">
                                            <option value="">Pilih Kategori</option>
                                            @foreach ($kategori as $ktgr)
                                            <option value="{{$ktgr->id}}">{{$ktgr->nama_kategori}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="jumlah">Jumlah</label>
                                        <input type="number" class="form-control col-8 "
                                            placeholder="Masukan Jumlah Barang" name="jumlah" value="{{old('jumlah')}}"
                                            required min="0">
                                    </div>
                                    <div class="form-group">
                                        <label for="keterangan">Keterangan</label>
                                        <input type="text" class="form-control col-8 " placeholder="Masukan Keterangan"
                                            name="keterangan" value="{{old('keterangan')}}" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end modal + supplier --}}
                <table class="table table-bordered" style="margin-top:1%">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">No</th>
                            <th scope="col" width="20%">Nama Barang</th>
                            <th scope="col" width="15%">Kategori</th>
                            <th scope="col" width="5%">Jumlah</th>
                            <th scope="col" width="20%">Keterangan</th>
                            <th scope="col" width="15%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($barang as $brg)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $brg->nama_barang }}</td>
                            <td>{{ $brg->kategori->nama_kategori }}</td>
                            <td>{{ $brg->jumlah_barang }}</td>
                            <td>{{ $brg->keterangan }}</td>
                            <td>
                                <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit barang-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Barang</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/edit-barang/{{$brg->id}}"
                                            enctype="multipart/form-data" onkeyup="validation()">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_barang">Nama Barang</label>
                                                <input type="hidden" class="form-control col-8" name="id"
                                                    value="{{$brg->id}}" required>
                                                <input type="text" class="form-control col-8"
                                                    placeholder="Masukan Nama Barang" name="nama_barang"
                                                    value="{{$brg->nama_barang}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="kategori">Kategori</label>
                                                <select class="col-8 form-control" name="id_kategori" required>
                                                    @foreach ($kategori as $ktgr)
                                                    <option value="{{$ktgr->id}}">{{$ktgr->nama_kategori}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="jumlah">Jumlah</label>
                                                <input type="number" class="form-control col-8"
                                                    placeholder="Masukan Jumlah" min="0" name="jumlah"
                                                    value="{{$brg->jumlah_barang}}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="keterangan">Keterangan</label>
                                                <input type="text" class="form-control col-8"
                                                    placeholder="Masukan Keterangan" name="keterangan"
                                                    value="{{$brg->keterangan}}" required>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus barang   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Barang</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/admin-inventori/delete-barang/{{$brg->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus {{$brg->nama_barang}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                {{$barang->links()}}
            </div>
        </div>
    </div>
</div>


@endsection
