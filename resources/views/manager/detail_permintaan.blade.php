@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Detail Permintaan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%" >No</th>
                            <th scope="col" width="20%">Nama Karyawan</th>
                            <th scope="col" width="20%">Tanggal Permintaan</th>
                            <th scope="col" width="15%">Nama Barang</th>
                            <th scope="col" width="15%">Jumlah Barang</th>
                            <th scope="col" width="10%">Status</th>
                            <th scope="col" width="15%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($detail_permintaan as $key => $data)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $data->nama_lengkap }}</td>
                            <td>{{ date('d F Y', strtotime($data->tgl_permintaan)) }}</td>
                            <td>{{ $data->nama_barang }}</td>
                            <td>{{ $data->jumlah_permintaan }}</td>
                            <td>{{ $data->status }}</td>
                            <td>
                                <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit permintaan-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Permintaan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/manager/transaksi/transaksi-edit-permintaan/detail/{{$data->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="status">Jumlah Permintaan</label>
                                                <input type="text" class="form-control" name="jumlah_permintaan" value="{{ $data->jumlah_permintaan }}">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus permintaan   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Permintaan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/manager/transaksi/transaksi-delete-permintaan/detail/{{$data->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus permintaan
                                            {{$data->nama_lengkap}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                {{$detail_permintaan->links()}}
            </div>
        </div>
    </div>
</div>
@endsection

