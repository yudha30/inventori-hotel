@extends('layouts.index')
@push('style')
    <script src="{{ asset('template/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('template/assets/vendor/toastr/toastr.min.js') }}"></script>
@endpush
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Penghapusan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Penghapusan
                </button> --}}
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif
                @if(count($cek_pengajuan)>0)
                    @foreach ($cek_pengajuan as $key => $value)
                        <script>
                            toastr.warning('Ada pengajuan penghapusan.', 'Peringatan');
                        </script>
                    @endforeach
                @endif
                <!-- Modal + penghapusan -->
                {{-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Penghapusan</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="/manager/transaksi/create-detail-penghapusan"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <input type="hidden" name="id_user"
                                            value="{{Auth::guard('manager')->user()->id}}">
                                    </div>
                                    <div class="permintaan">
                                        <div class="isi">
                                            <div class="form-group">
                                                <label for="barang">Inventori</label>
                                                <select class="col-8 form-control" name="id_inventori[]" required>
                                                    <option value="-">Pilih Inventori</option>
                                                    @foreach ($tb_inventori as $inv)
                                                    <option value="{{$inv->id}}">{{$inv->nama_inventori}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="add-list" data-inventori='{{$tb_inventori}}'>+
                                            Penghapusan</button>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-primary" id="btnTambah">Tambah</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div> --}}
                {{-- end modal + penghapusan --}}

                <table class="table table-bordered" style="margin-top: 1%">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Manager</th>
                            <th scope="col">Nama Inventori</th>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Ruangan</th>
                            <th scope="col">Keadaan Barang</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($detail_penghapusan as $dt_penghapusan)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $dt_penghapusan->nama_lengkap}}</td>
                            <td>{{ $dt_penghapusan->nama_inventori}}</td>
                            @if ($dt_penghapusan->barang_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $dt_penghapusan->nama_barang}}</td>
                            @endif
                            @if ($dt_penghapusan->ruangan_id==null)
                            <td>-</td>
                            @else
                            <td>{{ $dt_penghapusan->nama_ruangan}}</td>
                            @endif
                            <td>{{ $dt_penghapusan->keadaan_barang}}</td>
                            <td>{{ $dt_penghapusan->keterangan}}</td>
                            <td>
                                <a href="" class="btn btn-warning" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit perbaikan-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Penghapusan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/manager/transaksi/edit-penghapusan/{{$dt_penghapusan->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_inventori">Nama Inventori</label>
                                                <select class="col-8 form-control" name="id_inventori" required>
                                                    <option value="" selected>Pilih Inventori</option>
                                                    @foreach ($tb_inventori as $inv)
                                                    <option value="{{$inv->id}}" @if($inv->id == $dt_penghapusan->inventori_id) selected @endif>{{$inv->nama_inventori}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            {{-- @if ($dt_penghapusan->keterangan == 'Pengajuan Penghapusan') --}}
                                                <div class="form-group">
                                                    <label for="nama_inventori">Status</label>
                                                    <select class="col-8 form-control" name="status" required>
                                                        <option value="" selected>Pilih Status</option>
                                                        <option value="Acc Manager">Acc Manager</option>
                                                        <option value="Tidak Acc Manager">Tidak Acc Manager</option>
                                                    </select>
                                                </div>
                                            {{-- @endif --}}
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus perbaikan   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Penghapusan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/manager/transaksi/delete-penghapusan/{{$dt_penghapusan->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus ?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </tbody>
                </table>
                {{$detail_penghapusan->links()}}
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
<script>
    $(document).ready(function () {

        $(document).on('click', '#add-list', function (event) {
            event.preventDefault();

            let inventori = $(this).data('inventori');

            let element = `
            <div class="isi">
                <div class="form-group">
                    <label for="inventori">Inventori</label>
                    <select class="col-8 form-control" name="id_inventori[]" required>
                        <option value="">Pilih Inventori</option>
                        `;
            $.each(inventori, function (index, value) {
                element += `<option value='${value.id}'>${value.nama_inventori}</option>`
            });
            element += `
                    </select>
                </div>
            </div>
            `;

            $('.permintaan').append(element);

        });
    })

</script>
@endpush
