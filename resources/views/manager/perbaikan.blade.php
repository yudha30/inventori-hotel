@extends('layouts.index')
@push('style')
    <script src="{{ asset('template/assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('template/assets/vendor/toastr/toastr.min.js') }}"></script>
@endpush
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Perbaikan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    + Perbaikan
                </button> --}}
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif
                @if (count($status)>0)
                    @foreach ($status as $key => $value)
                        <script>
                            toastr.warning('{{$value->nama_barang}} tidak bisa diperbaiki.', 'Peringatan');
                        </script>
                    @endforeach
                @else

                @endif
                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">No</th>
                            <th scope="col" width="55%">Nama Karyawan</th>
                            <th scope="col" width="10%">Tanggal Perbaikan</th>
                            <th scope="col" width="10%">Status</th>
                            <th scope="col" width="20%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($tb_perbaikan as $prbaikan)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $prbaikan->user->nama_lengkap}}</td>
                            <td>{{ date('d F Y', strtotime($prbaikan->tgl_perbaikan))}}</td>
                            <td>{{ $prbaikan->status}}</td>
                            <td>
                                <a href="" class="btn btn-info" data-toggle="modal"
                                    data-target="{{'#detail'.$no}}">Detail</a>
                                <a href="" class="btn btn-warning" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit perbaikan-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Perbaikan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" action="/manager/transaksi/transaksi-edit-perbaikan/{{$prbaikan->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="nama_inventori">Keterangan</label>
                                                <select class="col-8 form-control" name="status" required>
                                                    <option value="{{$prbaikan->status}}">{{$prbaikan->status}}</option>
                                                    <option value="Acc Manager">Acc Manager</option>
                                                    <option value="Tidak Acc Manager">Tidak Acc Manager</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal detail permintaan-->
                        <div class="modal fade" id="{{'detail'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Detail Perbaikan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        @foreach ($detail_perbaikan as $dt_prbaikan)
                                            @if ($dt_prbaikan->perbaikan_id == $prbaikan->id)
                                                <div class="form-group">
                                                    <label for="nama_barang">Nama Inventori :
                                                        {{$dt_prbaikan->nama_inventori}}</label>
                                                    <br>
                                                @if ($dt_prbaikan->barang_id==null)
                                                    <label for="Barang">Barang : -</label>
                                                    <br>
                                                @else
                                                    <label for="Barang">Barang : {{ $dt_prbaikan->nama_barang }}</label>
                                                    <br>
                                                @endif
                                                @if ($dt_prbaikan->ruangan_id==null)
                                                    <label for="Ruangan">Ruangan : -</label>
                                                    <br>
                                                @else
                                                    <label for="Ruangan">Ruangan : {{ $dt_prbaikan->nama_ruangan }}</label>
                                                    <br>
                                                @endif
                                                    <label for="Ruangan">Keterangan : {{ $dt_prbaikan->keterangan }}</label>
                                                    <br>
                                                    @if ($dt_prbaikan->status)
                                                        <label for="Ruangan">Status : {{ $dt_prbaikan->status }}</label>
                                                    @else
                                                        <label for="Ruangan">Status : - </label>
                                                    @endif
                                                    <br>
                                                </div>
                                                <hr>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus perbaikan   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Perbaikan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/manager/transaksi/transaksi-delete-perbaikan/{{$prbaikan->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus perbaikan
                                            {{$prbaikan->nama_lengkap}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </tbody>
                </table>
                {{$tb_perbaikan->links()}}
            </div>
        </div>
    </div>
</div>


@endsection

@push('script')
<script>
    $(document).ready(function () {

        $(document).on('click', '#add-list', function (event) {
            event.preventDefault();

            let inventori = $(this).data('inventori');
            let element = `
            <div class="isi">
                <div class="form-group">
                    <label for="inventori">Inventori</label>
                    <select class="col-8 name="id_inventori[]" required>
                        <option>Pilih Inventori</option>
                        `;
            $.each(inventori, function (index, value) {
                element += `<option value='${value.id}'>${value.nama_inventori}</option>`
            });
            element += `
                    </select>
                </div>
            </div>
            `;

            $('.permintaan').append(element);
        });
    })
</script>
@endpush
