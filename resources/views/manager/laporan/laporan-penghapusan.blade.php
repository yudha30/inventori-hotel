@extends('layouts.index')

@section('content')
<h3 class="page-title">Data Penghapusan</h3>
<div class="container-fluid">
    <form action="{{ url('manager/laporan/cari-penghapusan') }}" method="POST" id="form-search">
        @csrf
        <div class="row">
            <div class="col-6" style="display: flex;">
                <div class="form-group" style="width:300px !important;display: flex">
                    <label style="padding-top: 5px !important ">Tanggal Awal</label>
                    <div class="input-group"  style="margin-left:5px !important;">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                        <input placeholder="masukkan tanggal Awal" autocomplete="off" type="text" class="form-control datepicker tanggal_awal"
                            name="tanggal_awal">
                    </div>
                </div>
                <div class="form-group" style="width:300px !important;display: flex">
                    <label style="padding-top: 5px !important ">Tanggal Akhir</label>
                    <div class="input-group"  style="margin-left:5px !important;">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                        <input placeholder="masukkan tanggal Akhir" autocomplete="off" type="text" class="form-control datepicker tanggal_akhir"
                            name="tanggal_akhir">
                    </div>
                </div>
                <div class="form-group" style="width:300px !important;display: flex">
                    <button type="submit" style="padding: 5px !important; width:100px !important; margin-left:10px !important" class="btn btn-primary search">Cari</button>
                </div>
            </div>
        </div>
        </form>
    <div class="row">
        <div class="col">
            <div class="panel">
                <div class="panel-heading" style="display: flex; justify-content: space-between;">
                    <h3 class="panel-title">Data</h3>
                    <a @if(!empty($tanggal_awal) && !empty($tanggal_akhir)) href="{{url('manager/laporan/cetak-penghapusan/')}}/{{$tanggal_awal}}/{{$tanggal_akhir}}" @else href="{{url('manager/laporan/cetak-penghapusan/0/0')}}" @endif class="btn btn-success">Cetak</a>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Lengkap</th>
                                <th>Inventori</th>
                                <th>keterangan</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key => $value)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $value->nama_lengkap }}</td>
                                    <td>{{ $value->nama_inventori }}</td>
                                    <td>{{ $value->keterangan }}</td>
                                    <td>{{ date('d F Y', strtotime($value->tgl_penghapusan)) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $( function() {
        $(".datepicker").datepicker({
            format: 'dd-mm-yyyy'
        }).val();
    } );

    $(document).on('click','.search',function(event){
        event.preventDefault();
        if($('.tanggal_awal').val() == '' || $('.tanggal_akhir').val() == ''){
            return toastr.error('Lengkapi Filter Tanggal','Gagal');
        }else{
            $('#form-search').submit();
        }
    })
</script>
@endpush
