@extends('layouts.index')

@section('content')
<h3 class="page-title">Data Barang</h3>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="panel">
                <div class="panel-heading" style="display: flex; justify-content: space-between;">
                    <h3 class="panel-title">Data Barang</h3>
                    <a href="{{ url('manager/laporan/cetak-barang') }}" class="btn btn-success">Cetak</a>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Barang</th>
                                <th>Jumlah</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $key => $value)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $value->nama_barang }}</td>
                                    <td>{{ $value->jumlah_barang }}</td>
                                    <td>{{ $value->keterangan }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $( function() {
        $(".datepicker").datepicker({
            format: 'dd-mm-yyyy'
        }).val();
    } );

    $(document).on('click','.search',function(event){
        event.preventDefault();
        if($('.tanggal_awal').val() == '' || $('.tanggal_akhir').val() == ''){
            return toastr.error('Lengkapi Filter Tanggal','Gagal');
        }else{
            $('#form-search').submit();
        }
    })
</script>
@endpush
