@extends('layouts.index')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Permintaan</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col">
                @if (session('status'))
                <div class="alert alert-success col-4 mt-2 mb-2">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('statusdel'))
                <div class="alert alert-danger col-4 mt-2 mb-2">
                    {{ session('statusdel') }}
                </div>
                @endif

                <table class="table table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%" >No</th>
                            <th scope="col" width="30%">Nama Karyawan</th>
                            <th scope="col" width="20%">Tanggal Permintaan</th>
                            <th scope="col" width="20%">Status</th>
                            <th scope="col" width="25%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $no=1;
                        @endphp
                        @foreach ($permintaan as $prmntaan)
                        <tr>
                            <td>{{$no++}}.</td>
                            <td>{{ $prmntaan->user->nama_lengkap }}</td>
                            <td>{{ date('d F Y', strtotime($prmntaan->tgl_permintaan)) }}</td>
                            <td>{{ $prmntaan->status }}</td>
                            <td>
                                <a href="{{ url('/manager/transaksi/transaksi-permintaan/detail') }}/{{ $prmntaan->id }}" class="btn btn-info btn-sm">Detail</a>
                                <a href="" class="btn btn-warning btn-sm" data-toggle="modal"
                                    data-target="{{'#edituser'.$no}}">Edit</a>
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="modal"
                                    data-target="{{'#hapus'.$no}}">Hapus</button>
                            </td>
                        </tr>

                        <!-- Modal Edit permintaan-->
                        <div class="modal fade" id="{{'edituser'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Data Permintaan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/manager/transaksi/transaksi-edit-permintaan/{{$prmntaan->id}}"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <select class="col-8 form-control" name="status" required>
                                                    <option value="{{$prmntaan->status}}">{{$prmntaan->status}}</option>
                                                    <option value="Acc Manager">Acc Manager</option>
                                                    <option value="Tidak Acc Manager">Tidak Acc Manager</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-primary">Edit</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- modal hapus permintaan   --}}
                        <div class="modal fade" id="{{'hapus'.$no}}" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Hapus Permintaan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post"
                                            action="/manager/transaksi/transaksi-delete-permintaan/{{$prmntaan->id}}"
                                            enctype="multipart/form-data">
                                            @method('delete')
                                            @csrf

                                            Apakah Yakin akan menghapus permintaan
                                            {{$prmntaan->user->nama_lengkap}}.?<br><br>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Batal</button>
                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
                {{$permintaan->links()}}
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $(document).ready(function () {

        $(document).on('click', '#add-list', function (event) {
            event.preventDefault();

            let barang = $(this).data('barang');
            let element = `
            <div class="isi">
                <div class="form-group">
                    <label for="barang">Barang</label>
                    <select class="col-8 form-control" name="id_barang[]" required>
                        <option>Pilih Barang</option>
                        `;
            $.each(barang, function (index, value) {
                element += `<option value='${value.id}'>${value.nama_barang}</option>`
            });
            element += `
                    </select>
                </div>

                <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" class="form-control col-8 "
                        placeholder="Masukan Jumlah Barang" name="jumlah[]"
                        required min="0">
                </div>
            </div>
            `;

            $('.permintaan').append(element);

        });
    })

</script>
@endpush
