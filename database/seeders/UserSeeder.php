<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_sistem = \App\Models\User::create([
            'username'  => 'admin',
            'password' => Hash::make('123456'),
            'nama_lengkap' => 'Admin',
            'no_hp' => '08089899',
            'level' => 1
        ]);

        $admin_inventori = \App\Models\User::create([
            'username'  => 'admin_inventori',
            'password' => Hash::make('123456'),
            'nama_lengkap' => 'Admin Inventori',
            'no_hp' => '08089899',
            'level' => 2
        ]);

        $manager = \App\Models\User::create([
            'username'  => 'manager',
            'password' => Hash::make('123456'),
            'nama_lengkap' => 'Manager',
            'no_hp' => '08089899',
            'level' => 3
        ]);

        $karyawan = \App\Models\User::create([
            'username'  => 'karyawan',
            'password' => Hash::make('123456'),
            'nama_lengkap' => 'Karyawan',
            'no_hp' => '08089899',
            'level' => 4
        ]);
    }
}
