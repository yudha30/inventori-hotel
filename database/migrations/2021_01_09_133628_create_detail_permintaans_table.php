<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPermintaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_detail_permintaan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('permintaan_id');
            $table->unsignedBigInteger('supplier_id');
            $table->unsignedBigInteger('barang_id');
            $table->integer('jumlah_permintaan');
            $table->string('status',50);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('permintaan_id')->references('id')->on('tb_permintaan');
            $table->foreign('supplier_id')->references('id')->on('tb_supplier');
            $table->foreign('barang_id')->references('id')->on('tb_barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_detail_permintaan');
    }
}
