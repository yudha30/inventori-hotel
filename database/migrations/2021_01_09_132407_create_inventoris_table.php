<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventorisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_inventori', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('barang_id')->nullable();
            $table->unsignedBigInteger('ruangan_id')->nullable();
            $table->string('nama_inventori',100);
            $table->string('keadaan_barang',100);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('barang_id')->references('id')->on('tb_barang');
            $table->foreign('ruangan_id')->references('id')->on('tb_ruangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_inventori');
    }
}
