<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPerbaikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_detail_perbaikan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perbaikan_id');
            $table->unsignedBigInteger('inventori_id');
            $table->text('keterangan')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('perbaikan_id')->references('id')->on('tb_perbaikan');
            $table->foreign('inventori_id')->references('id')->on('tb_inventori');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_detail_perbaikan');
    }
}
